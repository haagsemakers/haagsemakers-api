import { createMollieClient } from '@mollie/api-client';
import { config } from '../../config';

const mollieClient = createMollieClient({ apiKey: config.mollie.key });

class IMolliePaymentData {
  value: string
  description: string
  redirectUrl: string
  webhookUrl: string
}

const createMolliePayment = async (data: IMolliePaymentData) => {
  return await mollieClient.payments.create({
    amount: {
      value: data.value,
      currency: 'EUR'
    },
    description: data.description,
    redirectUrl: data.redirectUrl,
    webhookUrl: data.webhookUrl
  })
}

/**
 *
 * Fetch a payment from Mollie
 *
 **/
const getMolliePayment = async (id: string): Promise<any> =>{
  return await mollieClient.payments.get(id);
}

export {
  IMolliePaymentData,
  createMolliePayment,
  getMolliePayment
}
