# Docker setup and commands

## commands
Remove all dangling stuff:

    sudo docker prune

Start a container: docker run container/name
List containers: docker ps [-a]
Stop a container: docker stop [container id]
Delete a container: docker rm [container id]

https://linoxide.com/containers/stop-remove-clean-docker/

## Network
Create a network for access to the database by the apps.

    docker network create pg-network

for existing containers:

    docker network connect your-network-name container-name

## Volumes
Create volume for the database (to make data persistent)

    docker volume create pgdata

Remove dangling volumes

    sudo docker volume rm `sudo docker volume ls -q -f dangling=true`

## Database

    docker run --name postgres \
      --network pg-network \
      --restart always \
      -v database_data:/var/lib/postgresql/data \
      -p 5432:5432 \
      -e POSTGRES_USER=postgres \
      -e POSTGRES_PASSWORD=password \
      -d postgres:alpine


## Database admin

    docker run --name pgadmin \
      --network pg-network \
      --restart always \
      -p 5050:80 \
      -e "PGADMIN_DEFAULT_EMAIL=user@email.com" \
      -e "PGADMIN_DEFAULT_PASSWORD=admin" \
      -d  dpage/pgadmin4

## Useful commands

    # Enter container
    sudo docker exec -it hm_api sh

## API
### Local development
For local development we use docker compose

    docker-compose up

### production version
use docker compose
cd /var/www/haagsemakers/hm_api
docker-compose pull
docker-compose down
docker-compose up -d


For production, build the release and publish. (Re)build the docker container on the server.

    sudo docker stop hm_api
    sudo docker rm hm_api
    sudo docker run \
      --name hm_api \
      --network postgres \
      --restart always \
      --env-file=/var/www/haagsemakers/hm_api/.env \
      -p 8400:8400 \
      -d \
      registry.gitlab.com/haagsemakers/haagsemakers-api:master
