import * as express from 'express';
import { createQueryBuilder, getRepository } from 'typeorm';
import jwt from 'express-jwt';
import PostNotFoundException from './../../exceptions/PostNotFoundException';
import Controller from './../../interfaces/controller.interface';
import CreateEventDto from '../../models/event/event.dto';
import Event from '../../models/event/event.entity';
import logger from './../../utils/logger';
import { sanitize } from 'class-sanitizer';
import validateParamIdMiddleware from './../../middleware/validateParamId.middleware'
import validationMiddleware from './../../middleware/validation.middleware';
import { plainToClass } from 'class-transformer';
import { isAllowed } from './../../middleware/acl.middleware';
import HttpException from './../../exceptions/HttpException';
import User from '../../models/user/user.entity';
import Order from '../../models/order/order.entity';
// import { sendSimple } from './../../utils/email';
// import { emailEventCreated } from './../../utils/email/templates.mail';
import { getAttendees, hasOrder } from '../../services/event/event.service';
import { config } from './../../config';

class EventController implements Controller {

  public path = '/event';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getEvents);
    this.router.get(`${this.path}/:id`, validateParamIdMiddleware, this.getEvent);
    this.router.get(`${this.path}/:id/attendees`, jwt(config.auth0.jwtSettings), validateParamIdMiddleware, this.getEventAttendees);
    this.router.get(`${this.path}/:id/order`, jwt(config.auth0.jwtSettings), validateParamIdMiddleware, this.getEventOrder);

    // Event Organization
    this.router.post(`${this.path}/:id/organizer`, validateParamIdMiddleware, isAllowed('event', null, 'edit'), this.addOrganizer);
    this.router.post(`${this.path}/:id/editor`, validateParamIdMiddleware, isAllowed('event', null, 'edit'), this.addEditor);
    this.router.post(`${this.path}`, validationMiddleware(CreateEventDto), isAllowed('event', null, 'create'), this.createEvent);

    this.router.put(`${this.path}/:id`, validateParamIdMiddleware, validationMiddleware(CreateEventDto, true), isAllowed(Event, null, 'update'), this.updateEvent);

    this.router.delete(`${this.path}/:id/organizer/:organizationId`, validateParamIdMiddleware, isAllowed(Event, null, 'update'), this.removeOrganizer);
    this.router.delete(`${this.path}/:id/editor/:editorId`, validateParamIdMiddleware, isAllowed(Event, null, 'delete'), this.removeEditor);
    this.router.delete(`${this.path}/:id`, validateParamIdMiddleware, isAllowed(Event, null, 'delete'), this.deleteEvent);

  }

  /**
   *
   * List events
   *
   **/
  private async getEvents(_req: express.Request, res:express.Response, next: express.NextFunction) {

    let events: Event[];

    try {
      events = await getRepository(Event).find({
        relations: ['tickets']
      });
    } catch(err) {
      return next(new HttpException(500, err));
    }

    res.json(events);
  }

  /**
   *
   * Show a event
   *
   **/
  private async getEvent(req: express.Request, res:express.Response, next: express.NextFunction) {
    console.log('getEvent', req.params)
    let event: any;

    // Find the event
    try {
      event = await getRepository(Event).findOne({
        where: { id: req.params.id },
        relations: ['tickets']
      });
    } catch(err) {
      console.log(err);
      next(new PostNotFoundException(req.params.id));
    }

    // Add attendees to the event
    if (event) { event.attendees = await getAttendees(event.id);  }

    res.json(event);
  }

  /**
   *
   * Get event attendees
   *
   **/
  private async getEventAttendees(req: express.Request, res:express.Response) {
    const id = parseInt(req.params.id);
    const attendees = await getAttendees(id);

    res.json(attendees);
  }

  /**
   *
   * Get event attendees
   *
   **/
  private async getEventOrder(req: express.Request, res:express.Response) {
    const id = parseInt(req.params.id);
    if (!id) { return res.status(401).json(false); }

    const order: Order = await hasOrder(id, req.user.sub);

    return res.json(order);
  }


  /**
   *
   * Create a event
   *
   **/
  private async createEvent(req: express.Request, res:express.Response, next: express.NextFunction) {

    let event: Event;
    // Convert incoming body to new Order (Only exposed values in DTO)
    let eventData: CreateEventDto = plainToClass(CreateEventDto, req.body, { excludeExtraneousValues: true });
    sanitize(eventData);

    // Check if end is after start
    if (eventData.end < eventData.start) {
      return next(new HttpException(400, 'Event end date is before start date.'));
    }

    try {
      const user: User = await getRepository(User).findOne({ where: { sub: req.user.sub}});

      event = getRepository(Event).create(eventData);
      event = await getRepository(Event).save(event);

      // Add current user as editor
      await createQueryBuilder()
        .relation(Event, 'editors')
        .of({ id: event.id }) // event
        .add({ id: user.id }) // user

      // Send confirmation email to current user
      // const mailData = emailEventCreated({ event, user });
      // await sendSimple(mailData);

    } catch (err) {
      logger.warn('err', err);
      return next(new HttpException(400, err));
    }

    res.send(event);

  }

  /**
   *
   * Update a event
   *
   **/
  private async updateEvent(req: express.Request, res:express.Response) {
    let result: Event;

    console.log(req.body);
    // fetch event and update
    try {
      let event = await getRepository(Event).findOne(req.params.id)
      if (!event) { return res.status(403).json('Event not found'); }
      getRepository(Event).merge(event, req.body);
      console.log(event);
      sanitize(event);
      result = await getRepository(Event).save(event);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    // Send the result back
    res.send(result);
  }

  /**
   *
   * Delete a event
   *
   **/
  private async deleteEvent(req: express.Request, res:express.Response) {

    const id = parseInt(req.params.id);

    // Fetch the item and delete
    try {
      const eventToRemove = await getRepository(Event).findOne(id);
      await getRepository(Event).remove(eventToRemove);
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    return res.json(true);
  }

  /**
   *
   * Add an organization to an event
   *
   **/
  private async addOrganizer(req: express.Request, res: express.Response, next: express.NextFunction) {

    if (!req.body.organization_id) { return next(new HttpException(400, 'organization_id is required'))}

    try {
      const result = Number(req.body.organization_id);
      if (!result || (result<1) || isNaN(result)) {
        next(new HttpException(400, 'organization_id is not a positive integer'));
      }
    } catch(err) {
      next(new HttpException(400, 'Error checking organization_id param'));
    }

    // Try to add organization to event
    try {
      await createQueryBuilder()
        .relation(Event, 'organizers')
        .of([{ id: req.params.id }]) // Event
        .add({ id: req.body.organization_id }) // Organization
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(true);
  }

  /**
   *
   * Remove an organization from an event
   *
   **/
  private async removeOrganizer(req: express.Request, res: express.Response, next: express.NextFunction) {

    try {
      const result = Number(req.params.organizationId);
      if (!result || (result<1) || isNaN(result)) {
        next(new HttpException(400, 'organization_id is not a positive integer'));
      }
    } catch(err) {
      next(new HttpException(400, 'Error checking organization_id param'));
    }

    // Try to add organization to event
    try {
      await createQueryBuilder()
        .relation(Event, "organizers")
        .of([{ id: req.params.id }])
        .remove({ id: req.params.organizationId })
    } catch(err) {
      next(new HttpException(400, err));
    }

    res.json(true);
  }

  /**
   *
   * Add an organization to an event
   *
   **/
  private async addEditor(req: express.Request, res: express.Response, next: express.NextFunction) {

    if (!req.body.user_id) { return next(new HttpException(400, 'user_id is required'))}

    try {
      const result = Number(req.body.user_id);
      if (!result || (result<1) || isNaN(result)) {
        next(new HttpException(400, 'user_id is not a positive integer'));
      }
    } catch(err) {
      next(new HttpException(400, 'Error checking user_id param'));
    }

    // Try to add editor to event
    try {
      await createQueryBuilder()
        .relation(Event, 'editors')
        .of([{ id: req.params.id }]) // posts
        .add({ id: req.body.user_id }) // image
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(true);
  }

  /**
   *
   * Remove an organization from an event
   *
   **/
  private async removeEditor(req: express.Request, res: express.Response, next: express.NextFunction) {

    try {
      const result = Number(req.params.userId);
      if (!result || (result<1) || isNaN(result)) {
        next(new HttpException(400, 'user_id is not a positive integer'));
      }
    } catch(err) {
      next(new HttpException(400, 'Error checking user_id param'));
    }

    // Try to add organization to event
    try {
      await createQueryBuilder()
        .relation(Event, 'editors')
        .of([{ id: req.params.id }])
        .remove({ id: req.params.userId })
    } catch(err) {
      next(new HttpException(400, err));
    }

    res.json(true);
  }
}

export default EventController;
