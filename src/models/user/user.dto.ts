import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsOptional,
  IsString,
  Length,
  MaxLength
} from 'class-validator';
import {
  Escape,
  Trim
} from 'class-sanitizer';
import { Expose, Type } from 'class-transformer';
import UserVisibility from './user.entity';

class CreateUserDto {

  @Expose()
  @IsString()
  public auth0: string;

  @Expose()
  @IsEmail()
  public email: string

  @Expose()
  @IsString()
  @Length(3,25)
  @Escape()
  @Trim()
  public nickname: string

  @Expose()
  @IsString()
  @Length(3,25)
  public slug: string

  @Expose()
  @IsString()
  public picture: string

  @Expose()
  @IsString()
  public account_plan: string

  @Expose()
  @IsDate()
  @IsOptional()
  public account_plan_end: Date

  @Expose()
  @IsBoolean()
  public admin?: boolean

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(15)
  @Escape()
  @Trim()
  public firstname: string

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(20)
  @Escape()
  @Trim()
  public lastname: string

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(300)
  @Escape()
  @Trim()
  public bio: string

  @Expose()
  @IsOptional()
  @Type(()=> UserVisibility)
  public visibility: string

  @Expose()
  @IsOptional()
  @IsDate()
  public signed_up: Date

}

export default CreateUserDto;
