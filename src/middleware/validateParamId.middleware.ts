import * as express from 'express';
import HttpException from '../exceptions/HttpException';

function validateParamIdMiddleware(req: express.Request, _res: express.Response, next: express.NextFunction) {

  let result: number;

  try {
    result = Number(req.params.id);
    if (!result || (result<1) || isNaN(result)) {
      console.log('Value is not a positive integer');
      next(new HttpException(400, 'Value is not a positive integer'));
    }
  } catch(err) {
    next(new HttpException(400, 'Error checking id param'));
  }

  next();

}

export default validateParamIdMiddleware;
