import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import Order from './../order/order.entity';
import UserSocials from './user_socials.entity';
import UserProjects from './user_projects.entity';
import Interest from './interest.entity';
import Skill from './skill.entity';
import EventEditors from './../event/event_editors.entity';

export enum UserVisibility {
  HIDDEN = 'hidden',
  CLUB = 'club',
  AUTHENTICATED = 'authenticated',
  PUBLIC = 'public'
}

@Entity()
class User {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column({
    unique: true,
    select: false
  })
    auth0: string;

  @Column({
    unique: true,
    select: false
  })
    email: string;

  @Column({ unique: true })
    nickname: string;

  @Column({ unique: true })
    slug: string;

  @Column()
    picture: string

  @Column({ default: 'basic' })
    account_plan: string

  @Column({ nullable: true})
    account_plan_end: Date

  @Column({ default: false })
    admin: boolean

  @Column({ type: 'text', nullable: true})
    bio: string

  @Column({ nullable: true})
    firstname: string

  @Column({ nullable: true})
    lastname: string

  @Column({
    type: 'enum',
    enum: UserVisibility,
    default: UserVisibility.CLUB
  })
    visibility: string

  @Column({
    type: 'timestamptz'
  })
    signed_up: Date

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    created_at: Date

  @UpdateDateColumn({
      name: 'updated_at',
      type: 'timestamptz'
    })
    updated_at: Date

  // User orders, one-to-many (1 user can have multiple orders)
  @OneToMany(() => Order, order => order.user)
    orders: Order[];

  // User socials, one-to-many (1 user can have multiple socials)
  @OneToMany(() => UserSocials, social => social.user)
    socials: UserSocials[];

  // User projects, one-to-many (1 user can have multiple projects)
  @OneToMany(() => UserProjects, item => item.user)
    projects: UserProjects[];

  // User interests many-to-many (many users can have many interests)
  @ManyToMany(() => User, user => user.interests)
    interests: Interest[];

  // User skills many-to-many
  @ManyToMany(() => User, user => user.skills)
    skills: Skill[];

  // An event can have multiple editors with a role
  @OneToMany(() => EventEditors, (eventEditors) => eventEditors.user)
    public events!: EventEditors[];
}

export default User;
