import path from 'path';
import { Router, Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import Controller from '../../interfaces/controller.interface';
import logger from '../../utils/logger';
import HttpException from '../../exceptions/HttpException';
import validationMiddleware from '../../middleware/validation.middleware';
import looselyAuthenticatedMiddleware from '../../middleware/loose-jwt.middleware';
import CreateUserDto from '../../models/user/user.dto';
import User from '../../models/user/user.entity';
import {
  updateAuth0User,
  IAuth0User,
  nicknameIsAvailable,
  castToAuth0
} from '../../services/auth0';
import {
  fetchUser,
  updateUserSocials,
} from '../../services/user/user.service';
import Multer from 'multer';
import uuidv4 from 'uuid';
import fs from 'fs';
const gm = require('gm').subClass({imageMagick: true});
import { config } from '../../config';

// Set multer storage and filter
const storage = Multer.diskStorage({
  destination: function (_req:any, _file:any, cb) {
    cb(null, path.join(__dirname, '../../../files/avatar/'))
  },
  filename: function (_req:any, file, cb) {
    cb(null, uuidv4() + path.extname(file.originalname))
  }
});
const fileFilter = (_req: any, file: any, cb: any) => {
  const valid = [ 'image/png', 'image/jpg', 'image/jpeg' ];
  if (valid.indexOf(file.mimetype) !== -1) {
    cb(null, true);
  } else {
    cb(null, false);
  }
}
const upload = Multer({
  storage: storage,
  limits: {
    fileSize: 2*1024*1024,
    files: 1
  },
  fileFilter: fileFilter
});

interface MulterRequest extends Request {
  file: any;
}

class UserController implements Controller {

  public path = '/user';
  public router = Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {

    this.router
      .get(`${this.path}/me`      , this.getCurrentUser)
      .get(`${this.path}/search`  , looselyAuthenticatedMiddleware            , this.searchUser)

      .put(`${this.path}`         , validationMiddleware(CreateUserDto, true) , this.updateUserAccount)

      .post(`${this.path}/social` , this.updateSocials)
      .post(`${this.path}/avatar` , upload.single('avatar')                   , this.updateAvatar);
  }

  /**
   *
   * Create a new user if user does not yet exists locally.
   *
   **/
  private getCurrentUser = async (req: Request, res: Response, _next: NextFunction) => {

    let user: User;
    const sub: string = req.user.sub;
    
    try {
      user = await fetchUser(sub);
    } catch(err) {
      logger.warn(err);
    }

    res.json(user);
  }

  /**
   *
   * Update current user details. Store in localdb and update at auth0;

      user account, profile, socials, interests, skills, projects
   *
   **/
  private updateUserAccount = async (req: Request, res: Response, next: NextFunction) => {

    logger.debug(`[ user.controller updateUser ] Update current user. `);
    const user_id: string = req.user.sub;
    let user: User;
    let incoming: any = req.body;

    // Get current user
    try { user = await fetchUser(user_id);
    } catch(err) { return next(new HttpException(400, err)) }
    if (!user) { return next(new HttpException(400, 'No user found')); }

    // Validate if requested nickname is available
    if (incoming.nickname && (user.nickname !== incoming.nickname) ) {
      let available = await nicknameIsAvailable(incoming.nickname)
      if (!available) { return next(new HttpException(400, 'Nickname if not available. ')) }
    }

    // convert to auth0 object
    let casted: IAuth0User = castToAuth0(req.body);
    console.log('casted', casted);

    // Filter out unchanged
    let newAuth0: IAuth0User = {};

    if (casted.nickname && (casted.nickname !== user.nickname) ) {
      newAuth0.nickname = user.nickname;
    }
    if (casted.picture && (user.picture !== casted.picture) ) {
      newAuth0.picture = user.picture;
    }
    if ( casted.app_metadata ) {

      if ( casted.app_metadata.slug && (user.slug !== casted.app_metadata.slug)) {
        newAuth0.app_metadata = newAuth0.app_metadata || {};
        newAuth0.app_metadata.slug = casted.app_metadata.slug;
      }
    }
    if ( casted.user_metadata ) {

      if ( casted.user_metadata.firstname && (user.firstname !== casted.user_metadata.firstname)) {
        newAuth0.user_metadata = newAuth0.user_metadata || {};
        newAuth0.user_metadata.firstname  = casted.user_metadata.firstname;
      }
      if ( casted.user_metadata.lastname && (user.lastname !== casted.user_metadata.lastname)) {
        newAuth0.user_metadata = newAuth0.user_metadata || {};
        newAuth0.user_metadata.lastname   = casted.user_metadata.lastname;
      }
      if ( casted.user_metadata.bio && (user.bio !== casted.user_metadata.bio) ) {
        newAuth0.user_metadata = newAuth0.user_metadata || {};
        newAuth0.user_metadata.bio = casted.user_metadata.bio;
      }
      if ( casted.user_metadata.visibility && (user.visibility !== casted.user_metadata.visibility)) {
        newAuth0.user_metadata = newAuth0.user_metadata || {};
        newAuth0.user_metadata.visibility = casted.user_metadata.visibility;
      }
    }
    console.log('changed', newAuth0);

    // update at auth0
    const auth0Result = await updateAuth0User(user_id, newAuth0);
    console.log('authResult', auth0Result);

    // update local db
    let newUser: any = {};
    if (newAuth0.nickname) { newUser.nickname = newAuth0.nickname; }
    if (newAuth0.picture) { newUser.picture = newAuth0.picture; }
    if (newAuth0.user_metadata.firstname) { newUser.firstname = newAuth0.user_metadata.firstname; }
    if (newAuth0.user_metadata.lastname) { newUser.lastname = newAuth0.user_metadata.lastname; }
    if (newAuth0.user_metadata.bio) { newUser.bio = newAuth0.user_metadata.bio; }
    if (newAuth0.user_metadata.visibility) { newUser.visibility = newAuth0.user_metadata.visibility; }
    console.log('newUser localdb', newUser);

    try {
      const result = await getRepository(User).update(user.id, newUser);
      console.log(result);
    } catch(err) {
      console.warn(err);
    }

    res.json(true);
  }

  /**
   *
   * Update social links for a user.
   *
   **/
  private updateSocials = async (req: Request, res: Response, next: NextFunction) => {

    const user_id = req.user.sub;
    logger.debug(`updateSocials for ${user_id}. `);

    // update auth0
    try {
      await updateAuth0User(user_id, { user_metadata: { socials: req.body }});
      logger.debug(`[ user.controller updateSocials ] Socials updated at auth0 for user ${user_id}. `);
    } catch(err) {
      logger.warn(err);
      return next(new HttpException(400, err));
    }

    // update database
    try {
      await updateUserSocials(user_id, req.body);
      logger.debug(`[ user.controller updateSocials ] Socials updated in local db for user ${user_id}. `);
    } catch (err) {
      logger.warn(err);
      return next(new HttpException(400, err));
    }

    return res.json(true);
  }

  /**
   *
   * Fetch (a) user(s) based on search criteria.
   *
   **/
  private searchUser = async (req: Request, res: Response, _next: NextFunction) => {

    logger.info('[ account.controller search ] - Query for: ', req.query);

    // Make sure we browse appropriate visibility level for other users.
    let visibility = [ 'public' ];
    // First get currentuser status
    let user: User = null;
    if (req.user && req.user.sub) {
      console.log('fetch current user');
      user = await fetchUser(req.user.sub);
    }
    // Update browsing the visibility level
    if (user) {
      visibility.push('authenticated');
      if (user.account_plan === 'club') {
        visibility.push('club');
      }
    }
    console.log('visibility query: ', visibility);
    // // Validation
    let slug = req.query.slug;
    if ( slug && ( (slug.length<3) || (slug.length>50)) ) {
      slug = null;
    }

    //
    let users: User[];
    try {
      users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect("user.socials", "usersocial")
      .where('visibility = ANY(:visibility_array)', { visibility_array: visibility})
      .andWhere(slug ? `slug = :slug` : '1=1', { slug })
      .getMany();
    } catch(err) {
      console.warn(err);
    }

    //   .find({
    //   where: {
    //     // nickname: Like(`%${nickname}%`),
    //     // auth0: Not(req.user.sub)
    //     visibility: visibility
    //   },
    //   // select: ['nickname', 'picture', 'auth0'],
    //   // take:20
    // });

    console.log(users);

    return res.json(users);
  }

  /**
   *
   * Update a user's avatar
   *   req.file is the `avatar` file
   *   req.body will hold the text fields, if there were any
   *
   **/
  private updateAvatar = async (req: MulterRequest, res: Response, _next: NextFunction) => {

    const sub: string = req.user.sub;
    const newAvatar = req.file;
    const avatarPath = path.join(__dirname, `../../../files/avatar/`);

    gm(`${avatarPath}${newAvatar.filename}`)
      .resize(240, 240)
      .noProfile()
      .write(`${avatarPath}${newAvatar.filename}`, function (err: any) {
        if (err){ console.warn(err); }
        else { console.log('done'); }
      });


    if (!newAvatar) { return res.status(403).json({ error: 'no file'})}
    const fileUrl = `${config.api_url}/files/avatar/${newAvatar.filename}`;
    let oldAvatar;
    // get current avatar
    try {
      const user = await getRepository(User).findOne({
        where: {
          auth0: sub
        },
      });
      if (!user) return res.json(false);
      const oldAvatarPath = user.picture;
      oldAvatar = path.basename(oldAvatarPath);
    } catch(err) {
      console.warn('error saving picture', err);
      return res.json(false);
    }

    // Update user's avatar in local db
    try {
      await getRepository(User).update({
        auth0: sub
      }, { picture: fileUrl });
    } catch(err) {
      console.warn('error saving picture', err);
      return res.json(false);
    }
    // update user's picture at auth0
    try {
      await updateAuth0User(sub, { picture: fileUrl });
    } catch(err) {
      console.warn('error saving picture', err);
      return res.json(false);
    }
    // Try to remove previous image
    console.log('removing ' + oldAvatar);
    try {
      fs.unlinkSync(path.join(__dirname, `../../../files/avatar/${oldAvatar}`));
    } catch(err) {
      console.warn(err);
    }

    return res.json(true);
  }

  // // INTEREST ROUTES
  // private getInterests = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private createInterest = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private updateInterest = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
  // private deleteInterest = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
  //
  // // SKILL ROUTES
  // private getSkills = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private createSkill = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private updateSkill = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
  // private deleteSkill = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
  //
  // // PROJECT ROUTES
  // private getProjects = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private createProject = async (req: Request, res: Response, _next: NextFunction) => {
  //
  //   return res.json(null)
  // }
  // private updateProject = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
  // private deleteProject = async (req: Request, res: Response, _next: NextFunction) => {
  //   return res.json(null)
  // }
}

export default UserController;
