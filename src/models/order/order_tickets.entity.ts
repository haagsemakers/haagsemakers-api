// import {
//   Entity,
//   Column,
//   JoinColumn,
//   ManyToOne,
//   PrimaryGeneratedColumn } from "typeorm";
// import { IsInt, IsPositive } from 'class-validator';
// import Order from './order.entity';
// import Ticket from './../ticket/ticket.entity';
//
// @Entity()
// export default class OrderTickets {
//
//     @PrimaryGeneratedColumn()
//     public id!: number;
//
//     // public order_id!: number;
//     // public ticket_id!: number;
//
//     @Column()
//     @IsInt()
//     @IsPositive()
//     public amount!: number;
//
//     @ManyToOne(() => Order, order => order.tickets)
//     @JoinColumn({ name: 'order_id' })
//     public order_id!: Order;
//
//     @ManyToOne(() => Ticket, ticket => ticket.orders)
//     @JoinColumn({ name: 'ticket_id' })
//     public ticket_id!: Ticket;
// }
