import schedule from 'node-schedule';
import { syncUsers } from '../services/user/user.service';
import logger from '../utils/logger';

/**
 *
 * Synchronize the user details from auth0 to the local db.
 *
 **/
class SynchronizeUsersCron {

  name = 'synchronize_users';
  interval = '0 3 * * *';
  runAtStartup = false;

  constructor() {
    logger.info('[ cron: synchronize_users ] - Initiating Synchronize Users Cron, scheduling cron.');
    schedule.scheduleJob(this.name, this.interval, this.job);
    // Run this job at startup.
    if (this.runAtStartup) {
      this.job();
    }
  }

  // Initiate the cron
  public async job() {
    const result = await syncUsers();
    console.log('result', result)
    logger.info(`[ cron: synchronize users ] - Cron run done.`);
  }
}

export default SynchronizeUsersCron;
