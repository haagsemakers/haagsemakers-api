import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
  RemoveEvent
} from 'typeorm';
import urlSlug from 'url-slug';
import Event from '../models/event/event.entity';
import logger from '../utils/logger';
import Activity from '../models/activity/activity.entity';

@EventSubscriber()
export class HmEventSubscriber implements EntitySubscriberInterface<Event> {

  listenTo() {
    return Event;
  }

  // after insterting the event
  async afterInsert(event: InsertEvent<Event>) {
    logger.debug(`Event ${event.entity.id} inserted`);

    // Create slug
    event.entity.slug = 'event/' + urlSlug(event.entity.name) + '-' + event.entity.id;
    await event.manager.getRepository(Event).save(event.entity);
    logger.debug(`Event slug updated: ${ event.entity.slug }`);

    // Add activity to the DB (current event.manager has to be used because of transations)
    await event.manager.getRepository(Activity).save({ action: 'create', description: `Event ${ event.entity.id } created`, data: { event_id: event.entity.id }});

  }

  async afterUpdate(event: UpdateEvent<Event>) {
    await event.manager.getRepository(Activity).save({ action: 'update', description: `Event ${ event.entity.id } updated`, data: { event_id: event.entity.id }});
  }

  async afterRemove(event: RemoveEvent<Event>) {
    await event.manager.getRepository(Activity).save({ action: 'delete', description: `Event ${ event.entity.id } deleted`, data: { event_id: event.entity.id }});
  }
}
