import {
  Entity,
  Column,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn } from "typeorm";
import Event from './event.entity';
import User from './../user/user.entity';

export enum EventRole {
  ADMIN = 'admin',
  MANAGER = 'manager',
  EDITOR = 'editor',
}

@Entity()
export default class EventEditors {

    @PrimaryGeneratedColumn()
    public id!: number;

    // public event_id!: number;
    // public user_id!: number;

    @Column('enum', { enum: EventRole, default: EventRole.EDITOR })
    public role!: number;

    @ManyToOne(() => User, user => user.events)
    @JoinColumn({ name: 'user_id' })
    public user: User;

    @ManyToOne(() => Event, event => event.editors)
    @JoinColumn({ name: 'event_id' })
    public event!: Event;
}
