import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn } from 'typeorm';
import User from './user.entity';

@Entity()
class Skill {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
    name: string;

  @Column()
    description: string;

  @ManyToMany(() => User, user => user.skills)
  @JoinTable({
    name: 'user_skills',
    joinColumns: [
      { name: 'skill_id' }
    ],
    inverseJoinColumns: [
      { name: 'user_id' }
    ],
  })
  users: User;
}

export default Skill;
