import { Router, Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
import User from '../../models/user/user.entity';
import Activity from '../../models/activity/activity.entity';
import HttpException from '../../exceptions/HttpException';

class ActivitiesController implements Controller {

  public path = '/activity';
  public router = Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getActivities);
  }

  private async getActivities(req: Request, res: Response, next: NextFunction) {

    try {
      const user = await getRepository(User).findOne({
        where: {
          auth0: req.user.sub,
          admin: true
        }
      })
      if (!user) { return next(new HttpException(404, 'user not found'))}

      const list:Activity[] = await getRepository(Activity).find({
        take: 1000,
        order: { id: 'DESC' }
      });

      return res.json(list);
    } catch(err) {
      return next(new HttpException(400, err));
    }
  }
}

export default ActivitiesController;
