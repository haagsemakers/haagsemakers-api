import {
  IsOptional } from 'class-validator';
import {
  Expose,
  Type} from 'class-transformer';
import Ticket from './../ticket/ticket.entity';
import User from './../user/user.entity';
import OrderStatus from './../order/order.entity';

class CreateOrderDto {

  @Expose()
  @Type(() => Ticket)
  public ticket: Ticket

  @Expose()
  @Type(() => User)
  public user: User

  @Expose()
  @IsOptional()
  public notes: string

  @Expose()
  @IsOptional()
  @Type(() => Date)
  public time_remaining: Date

  @Expose()
  @IsOptional()
  @Type(()=> OrderStatus)
  public status: string

}

export default CreateOrderDto;
