# Haagse Makers Tickets API

[haagsemakers.nl website](https://haagsemakers.nl)  
[haagsemakers.nl API](https://haagsemakers.nl/api/tickets)  


## Local development
The easiest way for local development is to use [docker](https://docker.com) (Check the [Docker Requirements](https://docs.docker.com/docker-for-windows/install/)). By using docker-compose the required elements to run the API server are being set up: [TimescaleDB](https://www.timescale.com), [pgadmin](https://www.pgadmin.org) and the [node server](https://nodejs.org) running with [pm2](pm2.keymetrics.io).

    docker-compose up

The API is now live on [http://localhost:8100](http://localhost:8100) and the database administration (pgadmin) on [http://localhost:5050](http://localhost:8100)

## Deploy

    git push live master
