import {
  EventSubscriber,
  EntitySubscriberInterface,
  getRepository,
  InsertEvent,
  UpdateEvent } from 'typeorm';
import Activity from './../models/activity/activity.entity';
import Order from './../models/order/order.entity';
import Ticket from './../models/ticket/ticket.entity';
import Event from '../models/event/event.entity';
import User from '../models/user/user.entity';
import logger from '../utils/logger';
import {
  orderConfirmation,
  orderCancelation} from '../utils/email/templates'
import { sendSimple } from '../utils/email';

@EventSubscriber()
export class OrderSubscriber implements EntitySubscriberInterface<Order> {

  listenTo() {
    return Order;
  }

  /**
   *
   * After insterting the event
   *
   **/
  async afterInsert(order: InsertEvent<Order>) {

    logger.debug('[ order.subscriber ] afterInsert init');

    try {
      // Update ticket sold count
      // const u: User = order.entity.user;
      // const o: Order = order.entity;
      const t: Ticket = order.entity.ticket;
      t.sold = t.sold +1;
      await order.manager.getRepository(Ticket).save(t);
      logger.debug(`[ order.subscriber ] Order sold count updaed`);
      // Update event sold count
      const e: Event = order.entity.event;
      e.sold = e.sold +1;
      await order.manager.getRepository(Event).save(e);
      logger.debug(`[ order.subscriber ] Event sold count updated`);

      // logger.debug(`[ order.subscriber ] Creating activity`);
      await getRepository(Activity).save({ action: 'create', description: `Order ${order.entity.id} created`, data: { order_id: order.entity.id, ticket_id: t.id, event_id: e.id}});
    } catch(err) {
      console.warn('Error updating ticket count');
    }

  }

  /**
   *
   **/
  async afterUpdate(order: UpdateEvent<Order>) {

    logger.debug(`[ order.subscriber ] init `);

    if (!order.databaseEntity || !order.entity) { return; }

    const oldStatus = order.databaseEntity.status;
    const newStatus = order.entity.status;
    logger.debug(`[ order.subscriber ] Old status: ${ oldStatus }, New status: ${ newStatus }`);

    // Handle order completion
    if ((oldStatus === 'pending') &&  (newStatus === 'completed')) {
      logger.debug(`[ order.subscriber ] Handle completion of the order`);
      await this.handleOrderCompletion(order);
    }

    // Handle order cancellation
    if (
      ( (oldStatus === 'pending') || (oldStatus === 'completed') )
      && (newStatus === 'canceled')
    ) {
      logger.debug(`[ order.subscriber ] Handle cancellation of the order`);
      await this.handleOrderCancelation(order);
    }

    await getRepository(Activity).save({ action: 'update', description: `Order ${order.entity.id} updated from ${oldStatus} to ${newStatus}`, data: { order_id: order.entity.id}});
  }

  // Handle order cancelation
  handleOrderCancelation = async (order: UpdateEvent<Order>) => {
    try {
      // get ticket and event
      const o = await getRepository(Order).findOne({
        where: { id: order.entity.id },
        relations: ['user', 'ticket', 'event']
      });
      // Get email address of user
      const u = await getRepository(User).findOne({
        where: { id: o.user.id },
        select: [ 'email', 'nickname' ]
      })
      // Update ticket sold count
      const t: Ticket = o.ticket;
      t.sold = t.sold -1;
      await getRepository(Ticket).save(t);
      // Update event sold count
      const e: Event = o.event;
      e.sold = e.sold -1;
      await getRepository(Event).save(e);

      logger.debug(`[ order.subscriber ] Send confirmation email to user. `);
      const mailData = orderCancelation({
        to: {
          email: u.email,
          name: u.nickname
        },
        event: o.event,
        ticket: o.ticket
      });
      await sendSimple(mailData);
    } catch (err) {
      console.warn(err);
      logger.warn('[ order.subscriber ] error', err);
    }
  }

  // Handle order completion
  handleOrderCompletion = async (order: UpdateEvent<Order>) => {
    try {
      // get order details
      const o = await getRepository(Order).findOne({
        where: { id: order.entity.id },
        relations: ['user', 'ticket', 'event']
      });
      // Get email address of user
      const u = await getRepository(User).findOne({
        where: { id: o.user.id },
        select: [ 'email', 'nickname' ]
      })

      logger.debug(`[ order.subscriber ] Send confirmation email to user. `);
      const mailData = orderConfirmation({
        to: {
          email: u.email,
          name: u.nickname
        },
        event: o.event,
        ticket: o.ticket
      });
      await sendSimple(mailData);
    } catch(err) {
      console.warn('[ order.subscriber ] Error sending email', err);
    }
  }
}
