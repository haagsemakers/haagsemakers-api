import { getRepository } from 'typeorm';
import Payment from '../../models/payment/payment.entity';
import CreatePaymentDto from '../../models/payment/payment.dto';
import logger from '../../utils/logger';
import {config} from '../../config';
import { IMolliePaymentData, createMolliePayment} from '../../services/mollie';

/**
 *
 * Save a new payment in the database
 *
 **/
const createPayment = async (data: CreatePaymentDto): Promise<Payment> => {
  logger.debug(`[ payment.service ] - Starting to create a new payment for order ${ data.order.id }`);

  let payment: any;
  try {
    payment = getRepository(Payment).create(data);
    logger.debug(`[ payment.service ] - New payment created for order: ${ data.order.id }`);
    payment = await getRepository(Payment).save(payment);
    logger.debug(`[ payment.service ] - New payment saved for order ${ data.order.id } with payment_id: ${ payment.id }`);
  } catch (err) {
    console.warn(err);
    return Promise.reject(err);
  }

  // Create Mollie payment
  try {
    logger.debug('[ payment.service ] - Initiating new Mollie payment');
    const molliePaymentDto: IMolliePaymentData = {
      value: ((payment.cost + payment.fee)/100).toFixed(2),
      description: 'payment_id: ' + payment.id,
      webhookUrl: `${ config.api_url }/payment/mollie-hook`,
      redirectUrl: `${ config.url }/settings/orders`,
    }
    const molliePayment = await createMolliePayment(molliePaymentDto);
    logger.debug(`[ payment.service ] - molliePayment created with id: ${molliePayment.id}`);

    // Update payment with new mollie information
    logger.debug(`[ payment.service ] - Update payment status with molliepayment information`);
    payment.status = molliePayment.status;
    payment.mollie_id = molliePayment.id;
    payment.mollie_expires = molliePayment.expiresAt;
    payment.mollie_details = JSON.stringify(molliePayment);
    payment = await getRepository(Payment).save(payment);

    payment.paymentUrl = molliePayment.getPaymentUrl();
    logger.debug(`[ payment.service ] - Update payment ${payment.id} complete`);
  } catch (err) {
    console.warn(err);
    return Promise.reject(err);
  }

  return payment;
}

/**
 *
 * Update an existing payment in the database
 *
 **/
const updatePayment = async (paymentId: number, data: any): Promise<any> => {

  let payment = await getRepository(Payment).findOne(paymentId);
  if (!payment) {
    console.log('Payment not found')
    return Promise.reject('Payment not found');
  }
  try {
    const result = await getRepository(Payment).update(payment.id, data);
    console.log(result);
  } catch(err) {
    console.warn(err);
  }

  return payment;

}

export {
  createPayment,
  updatePayment
}
