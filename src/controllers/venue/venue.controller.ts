
import * as express from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
import CreateVenueDto from '../../models/venue/venue.dto';
import Venue from '../../models/venue/venue.entity';
import { sanitize } from 'class-sanitizer';
import validateParamIdMiddleware from './../../middleware/validateParamId.middleware'
import validationMiddleware from './../../middleware/validation.middleware';
import { plainToClass } from 'class-transformer';
import { isAllowed } from './../../middleware/acl.middleware';
import HttpException from './../../exceptions/HttpException';

class VenueController implements Controller {

  public path = '/venue';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getVenues);
    this.router.get(`${this.path}/:id`, validateParamIdMiddleware, this.getVenue);
    this.router.post(`${this.path}`,
      validationMiddleware(CreateVenueDto),
      isAllowed('venue', null, 'create'),
      this.createVenue);
    this.router.put(`${this.path}/:id`,
      validateParamIdMiddleware,
      validationMiddleware(CreateVenueDto, true),
      isAllowed(Venue, null, 'update'),
      this.updateVenue);
    this.router.delete(`${this.path}/:id`,
      validateParamIdMiddleware,
      isAllowed(Venue, null, 'delete'),
        this.deleteVenue);
  }

  /**
   *
   * List venues
   *
   **/
  private async getVenues(_req: express.Request, res:express.Response, next: express.NextFunction) {

    let venues: Venue[];

    try {
      venues = await getRepository(Venue).find();
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(venues);
  }

  /**
   *
   * Show a venue
   *
   **/
  private async getVenue(req: express.Request, res:express.Response, next: express.NextFunction) {

    let venue: Venue;

    // Find the venue
    try {
      venue = await getRepository(Venue).findOne({
        where: { id: req.params.id }
      });
    } catch(err) {
      return next(new HttpException(400, err));
    }

    // Return the result
    res.json(venue);
  }

  /**
   *
   * Create a venue
   *
   **/
  private async createVenue(req: express.Request, res:express.Response, next: express.NextFunction) {

    let venue: Venue;
    // Convert incoming body to new Order (Only exposed values in DTO)
    let venueData: CreateVenueDto = plainToClass(CreateVenueDto, req.body, { excludeExtraneousValues: true });
    sanitize(venueData);

    try {
      venue = getRepository(Venue).create(venueData);
      venue = await getRepository(Venue).save(venue);
    } catch (err) {
      return next(new HttpException(400, err));
    }

    res.json(venue);
  }

  /**
   *
   * Update a venue
   *
   **/
  private async updateVenue(req: express.Request, res:express.Response, next: express.NextFunction) {

    let result: Venue;

    // fetch venue and update
    try {
      let venue = await getRepository(Venue).findOne(req.params.id)
      if (!venue) { return res.status(403).json('Venue not found'); }
      getRepository(Venue).merge(venue, req.body);
      sanitize(venue);
      result = await getRepository(Venue).save(venue);
    } catch (err) {
      return next(new HttpException(400, err));
    }

    // Send the result back
    res.json(result);
  }

  /**
   *
   * Delete a venue
   *
   **/
  private async deleteVenue(req: express.Request, res:express.Response, next: express.NextFunction) {

    const id = parseInt(req.params.id);

    // Fetch the item and delete
    try {
      const toDelete = await getRepository(Venue).findOne(id);
      await getRepository(Venue).remove(toDelete);
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(true);
  }
}

export default VenueController;
