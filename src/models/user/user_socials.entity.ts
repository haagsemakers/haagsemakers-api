import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn } from 'typeorm';
import User from './../user/user.entity';
import {
  IsNumber,
  IsString } from 'class-validator';
import {
  Escape,
  Trim } from 'class-sanitizer';
import { Expose } from 'class-transformer';

export class CreateUserSocialDto {

  @Expose()
  @IsString()
  @Escape()
  @Trim()
  title: string;

  @Expose()
  @IsString()
  url: string;

  @Expose()
  @IsNumber()
  @Escape()
  @Trim()
  user: User;
}

@Entity()
class UserSocials {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
    title: string;

  @Column()
    url: string;

  // User socials
  @ManyToOne(() => User, user => user.socials)
  @JoinColumn({ name: 'user_id' })
  public user: User

}

export default UserSocials;
