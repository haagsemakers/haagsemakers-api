import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import User from './../user/user.entity';

@Entity()
class UserProjects {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
    title: string;

  @Column()
    description: string;

  @Column()
    url: string;

  @Column()
    picture: string

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    created_at: Date

  @UpdateDateColumn({
      name: 'updated_at',
      type: 'timestamptz'
    })
    updated_at: Date

  // User socials
  @ManyToOne(() => User, user => user.projects)
  @JoinColumn({ name: 'user_id' })
  public user: User

}

export default UserProjects;
