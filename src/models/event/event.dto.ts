import { Type } from 'class-transformer';
import {
  IsDate,
  // IsEnum,
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
  Length,
  Max,
  MaxLength } from 'class-validator';
import { Expose } from 'class-transformer';
import Organization from './../organization/organization.entity';
import User from './../user/user.entity';
import EventStatus from './event.entity';
import Venue from './../venue/venue.entity';

class CreateEventDto {

  @Expose()
  @IsString()
  @Length(5,150)
    name: string;

  @IsString()
  @IsOptional()
    slug: string;


  @Expose()
  @IsString()
  @IsOptional()
  @MaxLength(150)
    summary: string;

  @Expose()
  @IsString()
  @IsOptional()
  @MaxLength(500)
    description: string;

  @Expose()
  @IsOptional()
  @IsUrl()
    url: string;

  @Expose()
  @IsDate()
  @Type(()=>Date)
    start: Date

  @Expose()
  @IsDate()
  @Type(()=>Date)
    end: Date

  @Expose()
  @IsOptional()
  @IsDate()
  @Type(()=>Date)
    published: Date

  // @Expose() // When creating an event, status is set to published by default. Making the event live is a seperate call.
  @IsOptional()
  // @IsEnum(EventStatus) // IsEnum checking for strings seems to be not working at this point.
  @Type(()=> EventStatus)
    status: string

  @Expose()
  @IsInt()
  @IsPositive()
  @IsOptional()
  @Type(() => Number)
  public venue: Venue;

  @Expose()
  @IsString()
  @IsOptional()
    logo: string

  @Expose()
  @IsInt()
  @IsOptional()
  @IsPositive()
  @Type(() => Number)
    capacity: number

  // Maximum number of tickets per user
  @Expose()
  @IsInt()
  @IsOptional()
  @IsPositive()
  @Max(1)
  @Type(() => Number)
    max_tickets_per_user: number

  @Expose()
  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
    organizers: Organization[];

  @Expose()
  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
    editors: User[];


}

export default CreateEventDto;
