import {Column,Entity,JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import User from './user.entity';

@Entity()
class Interest {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
    name: string;

  @Column()
    description: string;

  @ManyToMany(() => User, user => user.interests)
  @JoinTable({
    name: 'user_interests',
    joinColumns: [
      { name: 'interest_id' }
    ],
    inverseJoinColumns: [
      { name: 'user_id' }
    ],
  })
  users: User
}

export default Interest;
