import {
  AfterLoad,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsEnum,
  IsInt,
  IsOptional,
  IsPositive,
  Length,
  MaxLength } from 'class-validator';

import {
  Escape,
  Trim } from 'class-sanitizer';
import Event from './../event/event.entity';
import Order from './../order/order.entity';

export enum TicketRoleRequired {
  VISITOR = 'visitor',
  AUTHENTICATED = 'authenticated',
  CLUB = 'club'
}

@Entity()
class Ticket {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  @Length(3, 150)
  @Escape()
  @Trim()
  public name: string;

  @Column({ nullable: true })
  @MaxLength(150)
  @Escape()
  @Trim()
  public description: string;

  @Column({ default: 0 })
  @IsInt()
  @IsPositive()
  public cost: number

  @Column({ default: 0 })
  @IsInt()
  @IsPositive()
  public fee: number

  @Column({
    type: 'enum',
    enum: TicketRoleRequired,
    default: TicketRoleRequired.AUTHENTICATED
  })
  @IsEnum(TicketRoleRequired)
  public role_required: string

  // @Column({ default: 1 })
  // @IsInt()
  // @IsPositive()
  // public minimum_quantity: number;
  //
  // @Column({ nullable: true, default: 1 })
  // @IsInt()
  // @IsPositive()
  // public maximum_quantity: number;

  @Column()
  @IsOptional()
  @IsInt()
  @IsPositive()
  public capacity: number;

  @Column({ default: 0 })
  @IsInt()
  @IsPositive()
  public sold: number;

  protected tickets_available: number

  @Column({
    type: 'timestamptz',
    nullable: true
  })
  public sales_start: Date

  @Column({
    type: 'timestamptz',
    nullable: true
  })
  public sales_end: Date

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  public createdAt: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  public updatedAt: Date

  // A Ticket belongs to 1 event
  @ManyToOne(() => Event, event => event.tickets)
  @JoinColumn({ name: 'event_id' })
  public event: Event;

  // A ticket belongs to multiple orders
  @OneToMany(() => Order, order => order.ticket)
  public orders: Order[];

  @AfterLoad()
  getTicketsAvailable() {
    this.tickets_available = this.capacity - this.sold
  }
}

export default Ticket;
