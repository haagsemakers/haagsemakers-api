import { getRepository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import moment from 'moment';
import {
  IAuth0User,
  ISocial,
  getAuth0User,
  getAuth0Users,
  updateAuth0User
} from './../../services/auth0';
import User from '../../models/user/user.entity';
import UserSocials from '../../models/user/user_socials.entity';
import CreateUserSocialDto from '../../models/user/user_socials.entity';
import CreateUserDto from '../../models/user/user.dto';
import urlSlug from 'url-slug'
import logger from '../../utils/logger';
import { config } from '../../config';
import { sendSimple, iEmailData } from '../../utils/email';
import { syncUsersLog } from '../../utils/email/templates';

/**
 *
 * Helper function to generat a unique nickname.
 *
 **/
async function generateUniqueNickname(nickname: string):Promise<string> {
  logger.debug(`[ user.service - generateUniqueNickname ] Init ${nickname}`);
  let i=0;
  let valid=false;
  let newNickname = nickname;

  while(!valid) {
    logger.debug(`[ user.service - generateUniqueNickname ] Check if nickname is already taken: ${newNickname}. `);
    const user = await getRepository(User).findOne({ where: { nickname: newNickname }});
    if (!user) {
      logger.debug(`[ user.service - generateUniqueNickname ] Nickname is not yet taken. `);
      valid = true;
    } else {
      logger.debug(`[ user.service - generateUniqueNickname ] Nickname is already taken. `);
      i++;
      newNickname = nickname + i;
      logger.debug(`[ user.service - generateUniqueNickname ] New nickname is ${newNickname}. `);
    }
  }

  logger.debug(`[ user.service - generateUniqueNickname ] Result: ${newNickname}. `);
  return newNickname;
}

/**
 *
 * Create a new user in the local database.
 *
 **/
async function createUser(newUser: CreateUserDto): Promise<User>  {
  //
  newUser = plainToClass(CreateUserDto, newUser);
  // validate incoming user data
  const errors = await validate(newUser);
  if (errors.length) {
    logger.warn('User create error', errors);
    return Promise.reject(errors);
  }
  // Start saving the new user
  let user: User = null;
  try {
    // Create a new db entry
    user = getRepository(User).create(newUser);
    // Save the new user
    await getRepository(User).save(newUser);
    logger.info(`saved new user ${user.nickname} (${user.email})`);
  } catch(err) {
    // Something went wrong, return error
    return Promise.reject(err);
  }
  // return the result
  return user;
}

// /**
//  *
//  * Sync users from auth0 to local db
//  *
//  */
// async function syncUserUp(user_id: string, data: CreateUserDto): Promise<boolean> {
//   let result: boolean;
//   if (!data || !data.auth0) {
//     logger.warn('No incoming data')
//     return Promise.reject(false);
//   }
//
//   try {
//     const user = getRepository(User).findOne({where: { auth0: data.auth0 }});
//     if (!user) { logger.warn('User not found', data.auth0); }
//     // Validate incoming data
//     let newData: any = {};
//     if (data.firstname) { newData.firstname = data.firstname; }
//     if (data.lastname) { newData.lastname = data.lastname; }
//     if (data.bio) { newData.bio = data.bio; }
//     if (data.visibility) { newData.visibility = data.visibility;}
//     // update app_metadata at auth0
//     // result = await updateAppMetadata(user_id, newData);
//     logger.debug(`Update app metadata result: ${result}`);
//   } catch(err) {
//     console.warn(`[ user.service - syncUserDown ]`, err)
//     return Promise.reject(err);
//   }
//
//   return result;
// }

/**
 *
 * Update an existing user. In local db and at auth0
 *
 **/
async function updateUser(user_id:string, values: CreateUserDto): Promise<User | boolean>  {
  let user: User;
  try {
    user = await getRepository(User).findOne({where:{auth0: user_id}});
    if (!user) {
      logger.warn(`No user found for ${user_id}`);
      return false;
    }
  } catch(err) {
    logger.warn(err);
    return false;
  }
  // update
  user = Object.assign(user, values);
  return user;
}

// Update social links if needed
const updateUserSocials = async (user_id: string, incoming: any): Promise<boolean> => {

  const user = await fetchUser(user_id);
  if (!user) { return false; }

  // Delete all old values for this user
  try {
    const result = await getRepository(UserSocials)
      .createQueryBuilder()
      .delete()
      .where('user_id = :userId', { userId: user.id })
      .execute();
    console.log(result);
  } catch(err) {
    console.warn(err);
  }
  // Add new values
  let socials = [];
  // authsocials for saving at auth0
  let authSocials: ISocial[] = [];
  for (let [key, value] of Object.entries(incoming)) {
    if (key && value) {
      const social = {
        title: String(key),
        url: String(value),
        user: user
      }
      const socialData: CreateUserSocialDto = plainToClass(CreateUserSocialDto, social);
      socials.push(socialData);
      const s: ISocial = { url: socialData.url, title: socialData.title };
      authSocials.push(s);
    }
  }
  // save usersocials items
  let userSocials = getRepository(UserSocials).create(socials);
  try {
    await getRepository(UserSocials).save(userSocials);
  } catch(err) {
    console.warn(err);
  }
  // Update auth0

  await updateAuth0User(user_id, { user_metadata: { socials: authSocials }})

  return true;
}

/**
 *
 * Find user and fetch from auth0 if not exists
 *
 **/
async function fetchUser(sub: string): Promise<User>  {
  let user: User;

  // Fetch user from database
  try {
    user = await getRepository(User).findOne({
      where: { auth0: sub },
      relations: ['socials']
    });
    if (!user) {
      logger.debug('[ user.service ] - User not found, get user from auth0');
      let auth0User = await getAuth0User(sub);
      if (auth0User) {
        logger.debug('[ user.service ] - User fetched from auth0, sync to local db');
        user = await syncUserDown(auth0User);
      }
    }
  } catch(err) {
    logger.warn(err);
    return Promise.reject(err);
  }

  return user;
}


/**
 *
 * Convert Auth0 user to local db user object
 *
 **/
async function castToUser(auth0user: IAuth0User): Promise<CreateUserDto> {

  logger.debug(`[ user.service - mapAuth0User ] Initiating user mapping. `);
  console.log(auth0user);

  // Make sure there is a nickname
  if (!auth0user.nickname) {
    logger.debug(`[ user.service - mapAuth0User ] Nickname is not present, creating from email address.`);
    let nick = auth0user.email.split('@')[ 0];
    logger.debug(`[ user.service - mapAuth0User ] New nickname created: ${nick}`);

    logger.debug(`[ user.service - mapAuth0User ] Generate a unique new nickname: ${nick}`);
    auth0user.nickname = await generateUniqueNickname(nick);
    logger.debug(`[ user.service - mapAuth0User ] New unique nickname created: ${nick}`);

    await updateAuth0User(auth0user.user_id, { nickname: auth0user.nickname })
  }

  auth0user.app_metadata = auth0user.app_metadata || {};
  auth0user.user_metadata = auth0user.user_metadata || {};

  if (!auth0user.app_metadata.slug) {
    auth0user.app_metadata.slug = urlSlug(auth0user.nickname);
    await updateAuth0User(auth0user.user_id, { app_metadata: { slug: auth0user.app_metadata.slug }} );
  }

  if (!auth0user.app_metadata.account_plan) {
    auth0user.app_metadata.account_plan = 'basic';
    await updateAuth0User(auth0user.user_id, { app_metadata: { account_plan: auth0user.app_metadata.account_plan }} );
  }

  if (!auth0user.user_metadata.visibility) {
    auth0user.user_metadata.visibility = 'club';
    await updateAuth0User(auth0user.user_id, { user_metadata: { visibility: auth0user.user_metadata.visibility }} );
  }

  // Create the new user object.
  const newUser: CreateUserDto = {
    auth0: auth0user.user_id,
    email: auth0user.email,
    nickname: auth0user.nickname,
    slug: auth0user.app_metadata.slug,
    firstname: auth0user.user_metadata.firstname,
    lastname: auth0user.user_metadata.lastname,
    bio: auth0user.user_metadata.bio,
    picture: auth0user.picture,
    account_plan: auth0user.app_metadata.account_plan,
    account_plan_end: auth0user.app_metadata.account_plan_end,
    admin: auth0user.app_metadata.admin || false,
    visibility: auth0user.user_metadata.visibility,
    signed_up: moment(auth0user.created_at).toDate()
  }

  logger.debug(`[ user.service - mapAuth0User ] New user DTO created`);
  console.log(newUser);

  return newUser;
}

/**
 *
 * Sync users from auth0 to local db
 *
 */
async function syncUserDown(auth0User: IAuth0User): Promise<User> {
  console.log('SyncUserDown', auth0User);
  let user: User;

  try {
    if (!auth0User) {
      logger.warn('authProfile not found')
      return Promise.reject('User not found');
    }
    logger.debug(`[ user.service - syncUserDown ] User ${auth0User.user_id} fetched from auth0`);
    let newUserDTO: CreateUserDto = await castToUser(auth0User);
    logger.debug('[ user.service - syncUserDown ] NewUser created, convert to user object. ');
    newUserDTO = plainToClass(CreateUserDto, newUserDTO, { excludeExtraneousValues: true });

    logger.debug('[ user.service - syncUserDown ] Validating new user. ');
    const errors = await validate(newUserDTO);
    if (errors.length) {
      logger.warn('[ user.service - syncUserDown ] User create error. ', errors);
      throw new Error(errors.toString());
    }
    logger.debug('[ user.service - syncUserDown ] Create new user. ');
    user = getRepository(User).create(newUserDTO);
    logger.debug('[ user.service - syncUserDown ] Save new user. ');
    user = await getRepository(User).save(user);
    logger.debug('[ user.service - syncUserDown ] New user saved. ');
  } catch(err) {
    console.warn(`[ user.service - syncUserDown ]`, err)
    return Promise.reject(err);
  }

  logger.debug('[ user.service - syncUserDown ] Returning saved user. ');
  return user;
}

/**
 *
 * Sync users from auth0 to local db
 *
 */
async function syncUsers(): Promise<Boolean> {

  let emailData: iEmailData = {
    type: 'userSyncReport',
  	to: config.admin.email,
  	content: {
      total: 0,
      synced: 0,
      syncedUsers: []
    }
  }

  logger.debug('Sync auth0 users with local db');
  let users: IAuth0User[] = await getAuth0Users();
  emailData.content.total = 0;

  if (!users) {
    logger.debug('No users fetched from auth0. Sync done');
    sendReport(emailData);
    return true;
  }

  emailData.content.total = users.length;

  async function initiateCreateUser(newUser: IAuth0User): Promise<User> {

    let user: User;

    try {
      // Fetch user from db if exists.
      let existingUser = await getRepository(User).findOne({ where: { auth0: newUser.user_id }});
      // Sync a new user.
      if (!existingUser) {
        logger.debug('User does not exist yet. Start sync');
        user = await syncUserDown(newUser);
        emailData.content.syncedUsers.push({ nickname: user.nickname, user_id: user.auth0 });
      } else {
        // Sync an existing user.
        logger.debug('User already exists. skip.');
        // user = await updateUser(existingUser.auth0, userDTO);
      }
    } catch (err) {
      logger.warn('User save error');
      console.warn(err);
      Promise.reject(err);
    }
    return user;
  }

  async function asyncForEach(array: any, callback: any) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  const start = async () => {
    await asyncForEach(users, async (user: IAuth0User) => {
      logger.debug('Start function - each user: ' + user.email);
      await initiateCreateUser(user);
    });

    logger.debug('Done syncing all users from auth0 to local db. ');
    sendReport(emailData);
    return true;
  }

  logger.debug('Start');
  start();
}

/**
 *
 *
 *
 */
async function sendReport(data: iEmailData): Promise<boolean> {
  const mailData = syncUsersLog(data);
  await sendSimple(mailData);
  return true;
}

//
export {
  createUser,
  updateUser,
  updateUserSocials,
  fetchUser,
  castToUser,
  syncUsers,
  syncUserDown
}
