import {
  Expose,
  Type} from 'class-transformer';
import {
  IsInt,
  IsPositive,
  IsString,
  Max,
  Min
} from 'class-validator';
import User from '../user/user.entity';
import Order from '../order/order.entity';

class CreatePaymentDto {

  @IsString()
  description: string

  @Expose()
  @Min(1)
  @Max(10000)
  cost: number

  @Min(1)
  @Max(10000)
  fee: number

  @Expose()
  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
    user: User;

  @Expose()
  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
    order: Order;
}

export default CreatePaymentDto;
