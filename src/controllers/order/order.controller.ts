import * as express from 'express';
import {
  Any,
  getRepository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import Controller from './../../interfaces/controller.interface';
import logger from './../../utils/logger';
import { sanitize } from 'class-sanitizer';
import validateParamIdMiddleware from './../../middleware/validateParamId.middleware'
import validationMiddleware from './../../middleware/validation.middleware';
import HttpException from './../../exceptions/HttpException';
import Order from '../../models/order/order.entity';
import User from '../../models/user/user.entity';
import Event from '../../models/event/event.entity';
import CreateOrderDto from '../../models/order/order.dto';
import Ticket from '../../models/ticket/ticket.entity';
import { createPayment } from '../../services/payment/payment.service';
import Payment from '../../models/payment/payment.entity';
import CreatePaymentDto from '../../models/payment/payment.dto';

class OrderController implements Controller {

  public path = '/order';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getOrders);
    this.router.get(`${this.path}/:id`, validateParamIdMiddleware, this.getOrder);
    this.router.post(`${this.path}`, validationMiddleware(CreateOrderDto), this.createOrder);
    this.router.put(`${this.path}/:id`, validateParamIdMiddleware, validationMiddleware(CreateOrderDto, true), this.updateOrder);
    this.router.delete(`${this.path}/:id`, validateParamIdMiddleware, this.deleteOrder);
  }

  /**
   *
   * List orders for current user
   *
   **/
  private async getOrders(req: express.Request, res:express.Response, next: express.NextFunction) {

    const user = await getRepository(User).findOne({ where: { auth0: req.user.sub }});
    if (!user) { return next( new HttpException(403, 'User not found. ')); }

    const orders = await getRepository(Order)
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.event', 'event')
      .leftJoinAndSelect('order.payments', 'payment')
      .leftJoinAndSelect('order.ticket', 'ticket')
      .where('order.user_id = :id', { id: user.id })
      .orderBy('order.order_date', 'DESC')
      .getMany();

    return res.json(orders);

  }

  /**
   *
   * Show a order
   *
   **/
  private async getOrder(req: express.Request, res:express.Response) {

    let order: Order;

    try {
      order = await getRepository(Order).findOne({
        where: { id: req.params.id },
        relations: [ 'event', 'ticket', 'payments' ]
      });
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(order);
  }

  /**
   *
   * Create a order
   *
   **/
  private async createOrder(req: express.Request, res:express.Response, next: express.NextFunction) {

    /**
      Validate
        - User exists
        - Ticket exists
        - Event exists

        - User has required role for ticket ticket.role_required;
            user.hasRole(ticket.role_required);
        - Event status is 'live';
            event.status === 'live';
        - Ticket is avaiable
            ticket.capacity - ticket.sold > 0;
        - Ticket sales is active: sales_start >> current_data << sales_end
            moment.data(date).isBetween(start, end)
        - User doesn't have a ticket already
            ticket.findOne(where ticket_id===1 && user_id===1)
    **/


    let response: any = {};

    logger.debug('Create new order', req.body);
    if (!req.body.ticket_id) {
      return next(new HttpException(404, 'No ticket_id provided.'));
    }
    let user: User, ticket: Ticket, event: Event;

    // Get current user details
    user = await getRepository(User).findOne({ where: { auth0: req.user.sub }});
    if (!user) { return next( new HttpException(403, 'User not found. ')); }
    logger.debug('User found');

    // Get ticket
    ticket = await getRepository(Ticket).findOne({ where: { id: req.body.ticket_id }, relations: [ 'event' ]});
    if (!ticket) { return next( new HttpException(403, 'Ticket not found. ')); }
    logger.debug('Ticket found');

    // Get event
    event = await getRepository(Event).findOne({ where: { id: ticket.event.id }});
    if (!event) { return next( new HttpException(403, 'Event not found. ')); }
    logger.debug('Event found');

    // Check if user has required role
    switch (ticket.role_required) {
      case 'club':  // check user role
        break;
      case 'authenticated':
        if (!user.id) { return next(new HttpException(403, 'User is not authenticated. Please login. ')); }
        break;
      case 'visitor':
        // Always true
        break;
      default:
    }

    // Check if event status is live
    if (event.status !== 'live') {
      return next(new HttpException(403, 'Event is not live.'));
    }
    logger.debug('Event is live, continue');

    // Check if there are tickets available
    if ((ticket.capacity - ticket.sold) < 1) {
      return next(new HttpException(403, 'No tickets available for this event.'));
    }
    logger.debug('Ticket is available, continue');

    // Check if ticket sales is now
    const now = new Date();
    if (
      (
        (ticket.sales_end && now.getTime() >= ticket.sales_end.getTime())
        ||
        (ticket.sales_start && now.getTime() <= ticket.sales_start.getTime())
      )
    ) {
      return next(new HttpException(403, 'Ticket is not in sale now.'));
    }
    logger.debug('Ticket is in sale now, continue.');

    // Check if user doesn't have a ticket already
    try {
      const userHasTicket = await getRepository(Order).findOne({
        where: {
          ticket_id: ticket.id,
          user_id: user.id,
          status: Any([ 'draft', 'pending', 'completed' ])
        }
      });
      logger.debug('User already has ticket? ', userHasTicket);
      if (userHasTicket) { return next(new HttpException(403, 'User already has a ticket')); }
      logger.debug('User does not have a ticket yet, continue');
    } catch(err) {
      console.warn(err);
      return next(new HttpException(400, 'Error checking if user already has an order'));
    }

    // Create order
    let order: Order;
    try {
      let orderDto: CreateOrderDto = plainToClass(CreateOrderDto, req.body);
      logger.debug('order data created', orderDto);
      order = getRepository(Order).create(orderDto);
      order.time_remaining = null;
      order.user = user;
      order.ticket = ticket;
      order.event = event;
      if ( (ticket.cost + ticket.fee) > 0) {
        order.status='pending';
      } else {
        order.status = 'completed';
      }
      order = await getRepository(Order).save(order);
    } catch(err) {
      console.warn(err);
      return next(new HttpException(400, 'Error creating order'));
    }
    response.order = order;
    logger.debug('Order created and saved');

    // Create payment if needed
    if (ticket.cost || ticket.fee) {
      let payment: Payment;
      const paymentDto: CreatePaymentDto = {
        cost: ticket.cost,
        fee: ticket.fee,
        description: ticket.name,
        order: order,
        user: user
      }
      try {
        payment = await createPayment(paymentDto);
      } catch(err) {
        console.warn(err);
      }
      if (!payment) { console.warn('No payment created'); }

      response.payment = payment;
    }

    res.json(response);
  }

  /**
   *
   * Update a order
   *
   **/
  private async updateOrder(req: express.Request, res:express.Response, next: express.NextFunction) {

    logger.debug(`[ order.controller updateOrder] Updating order. ` + JSON.stringify(req.body));

    let order: Order;
    let user: User;

    try {
      // Find user
      user = await getRepository(User).findOne({ where: { auth0: req.user.sub }});
      if (!user) { return next(new HttpException(404, 'No user found')); }
      // Find the order
      order = await getRepository(Order).findOne({
        where: {
          id: req.params.id,
          user: user
        },
        relations: ['user, event']
      });
      if (!order) { return next(new HttpException(404, 'No order found')); }
      getRepository(Order).merge(order, req.body);
      sanitize(order);
      console.log('sanitized order', order);
      order = await getRepository(Order).save(order);
    } catch (err) {
      return next(new HttpException(500, err));
    }

    logger.debug(`[ order.controller updateOrder] Order updated`);

    res.json(order);
  }

  /**
   *
   * Delete a order
   *
   **/
  private async deleteOrder(req: express.Request, res:express.Response, next: express.NextFunction) {

    const id = parseInt(req.params.id)

    try {
      const order = await getRepository(Order).findOne(id);
      order.status = 'canceled';
      await getRepository(Order).save(order);
    } catch(err) {
      return next(new HttpException(500, err));
    }

    return res.json(true);
  }

}

export default OrderController;
