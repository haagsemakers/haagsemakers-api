import {
  Expose,
  Type} from 'class-transformer';
import {
  IsOptional,
  IsString
} from 'class-validator';
import ActivityAction from './activity.entity';
import Event from '../event/event.entity';
import Order from '../order/order.entity';
import Organization from '../organization/organization.entity';
import Payment from '../payment/payment.entity';
import Ticket from '../ticket/ticket.entity';
import User from '../user/user.entity';
import Venue from '../venue/venue.entity';

class CreateActivityDto {

  @Expose()
  @IsOptional()
  @Type(()=> ActivityAction)
  action: string

  @Expose()
  @IsString()
  description: string

  @Expose()
  @IsOptional()
  data?: any

  @Expose()
  @IsOptional()
  @Type(() => Event)
  event?: Event

  @Expose()
  @IsOptional()
  @Type(() => Order)
  order?: Order

  @Expose()
  @IsOptional()
  @Type(() => Organization)
  organization?: Organization

  @Expose()
  @IsOptional()
  @Type(() => Payment)
  payment?: Payment

  @Expose()
  @IsOptional()
  @Type(() => Ticket)
  ticket?: Ticket

  @Expose()
  @IsOptional()
  @Type(() => User)
  user?: User

  @Expose()
  @IsOptional()
  @Type(() => Venue)
  venue?: Venue
}

export default CreateActivityDto;
