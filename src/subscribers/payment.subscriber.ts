import {
  EventSubscriber,
  EntitySubscriberInterface,
  getRepository,
  InsertEvent,
  RemoveEvent,
  UpdateEvent } from 'typeorm';
import Activity from '../models/activity/activity.entity';
import Payment from '../models/payment/payment.entity';
import Order from '../models/order/order.entity';
import logger from '../utils/logger';

@EventSubscriber()
export class PaymentSubscriber implements EntitySubscriberInterface<Payment> {

  listenTo() {
    return Payment;
  }

  async afterInsert(event: InsertEvent<Payment>) {
    await event.manager.getRepository(Activity).save({ action: 'create', description: `Payment ${ event.entity.id } created`, data: { payment_id: event.entity.id }});
  }

  // After updating the payment
  async afterUpdate(payment: UpdateEvent<Payment>) {
    //
    if (!payment.databaseEntity || !payment.entity) { return; }

    const oldStatus = payment.databaseEntity.status;
    const newStatus = payment.entity.status;
    logger.debug(`[ payment.subscriber ] afterUpdate - handle status change. Old value: ${ oldStatus }, New vale: ${ newStatus }`);
    
    // Update the status of the associated order if the payment changed
    if ((oldStatus === 'open') && (newStatus === 'paid')) {
      try {
        const p = await getRepository(Payment).findOne({
          where: { id: payment.entity.id },
          relations: [ 'order' ]
        });
        const order = p.order;
        order.status = 'completed';
        await getRepository(Order).save(order);
        logger.debug('[ payment.subscriber ] afterUpdate. Order status updated to completed');
      } catch(err) {
        logger.warn(err);
      }
    }

    await payment.manager.getRepository(Activity).save({ action: 'create', description: `Payment ${ payment.entity.id } updated from ${oldStatus} to ${newStatus}`, data: { payment_id: payment.entity.id }});
  }

  async RemoveEvent(event: RemoveEvent<Payment>) {
    await event.manager.getRepository(Activity).save({ action: 'delete', description: `Payment ${ event.entity.id } deleted`, data: { payment_id: event.entity.id }});
  }
}
