import { NextFunction, Request, Response } from 'express';
import HttpException from './../exceptions/HttpException';

function errorMiddleware(error: HttpException, _req: Request, res: Response, _next: NextFunction) {

  console.log('error middleware:', error);
  console.log(error.message);
  const status: number = error.status || 500;
  const message: string = error.message || 'Something went wrong';

  res.status(status).json({ message, status });
}

export default errorMiddleware;
