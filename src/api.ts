import * as Sentry from '@sentry/node';
import bodyParser from 'body-parser';
import express from 'express';
import Controller from './interfaces/controller.interface';
import errorMiddleware from './middleware/error.middleware';
import helmet from 'helmet';
import cors from 'cors';
import jwt from 'express-jwt';
import { config } from './config';


export default class App {

  private app: express.Application;
  private controllers: Controller[];

  constructor(_controllers: Controller[]) {
    this.controllers = _controllers;
  }

  async initialize(): Promise<boolean> {
    this.app = express();
    
    this.initializeMiddlewares();
    this.initializeControllers(this.controllers);
    this.initializeErrorHandling();

    return true;
  }

  public listen() {
    this.app.listen(process.env.NODE_PORT, () => {
      console.log(`App listening on the port ${process.env.NODE_PORT}`);
    });
  }

  public getServer() {
    return this.app;
  }

  private initializeMiddlewares() {

    if (config.sentry.dsn) {
      console.log('[api] - Initializing sentry middleware');
      this.app.use(Sentry.Handlers.requestHandler());
    }

    // this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: '5mb' }));

    // Use helmet to secure Express headers
    let SIX_MONTHS = 1000*60*60*24*31*6;
    this.app.use(helmet.frameguard());
    this.app.use(helmet.xssFilter());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.ieNoOpen());
    this.app.use(helmet.hsts({maxAge: SIX_MONTHS, includeSubDomains: true, force: true}));
    this.app.use(helmet.noCache());
    this.app.disable('x-powered-by');

    this.app.use(cors({ maxAge: 1728000 }));

    // Use JWT
    let jwtCheck: any = jwt(config.auth0.jwtSettings);
    this.app.use(jwtCheck.unless({
      path: [
        { url: '/', methods: [ 'GET' ] },
        { url: /\/ticket*/, methods: [ 'GET' ] },
        { url: /\/event*/, methods: [ 'GET' ] },
        { url: '/hooks/gitlab-website' },
        { url: '/user/search', methods: [ 'GET' ] },
        { url: /\/files*/, methods: [ 'GET' ] },
        { url: '/payment/mollie-hook', methods: [ 'POST' ] },
      ]
    }));

    // initiate auth service
    require('./services/auth0/index');

    // Server static files
    this.app.use('/files', express.static('files'));
  }

  private initializeErrorHandling() {

    // Use sentry if available
    if (config.sentry.dsn) {
      console.log('[api] - Initializing sentry Error handling');
      this.app.use(Sentry.Handlers.errorHandler());
    }

    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
}
