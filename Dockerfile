FROM node:lts-alpine
MAINTAINER Arn van der Pluijm <https://gitlab.com/avdp>

WORKDIR /app

RUN apk update \
    && apk add --no-cache git\
    && apk add --no-cache bash \
    && apk --update add imagemagick \
    && npm install --quiet typescript -g \
    && npm install --quiet pm2 -g

COPY package.json package-lock.json ./
RUN npm install

COPY . .

RUN npm run build --production

EXPOSE 8400
