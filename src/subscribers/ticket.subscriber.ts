import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
  RemoveEvent
} from 'typeorm';
import Ticket from '../models/ticket/ticket.entity';
import logger from '../utils/logger';
import Activity from '../models/activity/activity.entity';

@EventSubscriber()
export class TicketSubscriber implements EntitySubscriberInterface<Ticket> {

  listenTo() {
    return Ticket;
  }

  /**
   *
   * after insterting the ticket
   *
   **/
  async afterInsert(event: InsertEvent<Ticket>) {
    logger.debug(`[ ticket.subscriber ] Ticket ${event.entity.id} created`);

    await event.manager.getRepository(Activity).save({ action: 'create', description: `Ticket ${ event.entity.id } created`, data: { ticket_id: event.entity.id }});
  }

  /**
   *
   * after updating the ticket
   *
   **/
  async afterUpdate(event: UpdateEvent<Ticket>) {
    logger.debug(`[ ticket.subscriber ] Ticket ${event.entity.id} updated`);

    await event.manager.getRepository(Activity).save({ action: 'update', description: `Ticket ${ event.entity.id } updated`, data: { ticket_id: event.entity.id }});
  }

  /**
   *
   * After deleting the ticket
   *
   **/
  async afterRemove(event: RemoveEvent<Ticket>) {
    logger.debug(`[ ticket.subscriber ] Ticket ${event.entity.id} deleted`);

    await event.manager.getRepository(Activity).save({ action: 'delete', description: `Ticket ${ event.entity.id } deleted`, data: { ticket_id: event.entity.id }});
  }
}
