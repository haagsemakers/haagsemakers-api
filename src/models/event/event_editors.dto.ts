import { Type } from 'class-transformer';
import {
  IsEnum,
  IsInt,
  IsPositive } from 'class-validator';
import { Expose } from 'class-transformer';
import User from './../user/user.entity';
import EventRole from './event_editors.entity';

class EventEditorsDto {

  @Expose()
  @IsInt()
  @IsPositive()
    event_id!: number;

  @Expose()
  @Type(()=> User)
    user: User;

  @IsEnum(EventRole)
  @Type(() => EventRole)
   role: number;


}

export default EventEditorsDto;
