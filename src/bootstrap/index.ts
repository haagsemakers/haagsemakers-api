import { Connection } from 'typeorm';
import { databaseConnection } from './../bootstrap/database-connection';
import { config } from './../config';

let database: Connection;

// Bootstrap the application async
export const bootstrap = async (): Promise<Boolean> => {
  console.log('Bootstrap - Start');

  try {
    const dbSync = config.db.sync;

    // create connection with database
    database = await databaseConnection();
    console.log('Bootstrap - databaseConnection created');

    await database.runMigrations();
    console.log('Bootstrap - Migrations ran');
    if (dbSync) {
      await database.synchronize(true)
      console.log('Bootstrap - Synchronize done');
    }

  } catch (err) {
    console.log('Bootstrap - error', err);
  }

  console.log('Bootstrap - done');

  return true;
};

export const Database: Connection = database;
