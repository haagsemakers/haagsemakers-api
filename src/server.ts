import 'reflect-metadata';
import { bootstrap } from './bootstrap';
import App from './api';
import * as controllers from './controllers';
import Controller from './interfaces/controller.interface';
import { initCron } from './cron';

bootstrap().then(async _connection => {

  // initialize controllers
  let controllersArray: Controller[] = [];
  Object.keys(controllers).forEach(key => {
    controllersArray.push( new (<any>controllers)[ key]() );
  });

  // Initialize API
  const app = new App(controllersArray);
  await app.initialize();
  app.listen();

  // initialize cron
  initCron();

}).catch(error => console.log('server error: ', error));
