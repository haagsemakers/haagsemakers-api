# Deploy
This page describes the process of deploying a new version of the backend service. All code is hosted on gitlab in the [Haagse Makers API registry](https://gitlab.com/haagsemakers/haagsemakers-api).
The API is hosted on a digital ocean droplet. Docker is used to run the different elements of the system:

  * postgres database
  * pgadmin database administration
  * node API (registry.gitlab.com/haagsemakers/haagsemakers-api)

More details are described in the docker documentation page.

At this point, we create a new docker build manually. (in the future this should be done automatically after each master branch push / new tag).

## Creating a new build
Always version the build. (version number, or 'master')

    sudo docker build -f Dockerfile.production -t registry.gitlab.com/haagsemakers/haagsemakers-api:${VERSION} .
    sudo docker login -u ${GITLAB_USER} -p ${GITLAB_TOKEN} registry.gitlab.com
    sudo docker push registry.gitlab.com/haagsemakers/haagsemakers-api:${VERSION}

## Updating the docker container on production

    cd /var/www/haagsemakers/hm_api
    docker-compose pull
    docker-compose down
    docker-compose up --build -d

Prerequisites:
  - Username: Gitlab username
  - Password: [Gitlab Personal Access Token](https://gitlab.com/profile/personal_access_tokens)

On the server update the container run:

    sudo docker login -u ${GITLAB_USER} -p ${GITLAB_TOKEN} registry.gitlab.com
    sudo docker pull registry.gitlab.com/haagsemakers/haagsemakers-api:latest

    sudo docker stop hm_api
    sudo docker rm hm_api
    sudo docker run \
      --name hm_api \
      --network pg-network \
      --restart always \
      --env-file=/var/www/haagsemakers/api/config/env \
      -p 8400:8400 \
      -d \
      registry.gitlab.com/haagsemakers/haagsemakers-api:master
