
import * as express from 'express';
import { getRepository } from 'typeorm';
import Controller from './../../interfaces/controller.interface';
import CreateOrganizationDto from '../../models/organization/organization.dto';
import Organization from '../../models/organization/organization.entity';
import { sanitize } from 'class-sanitizer';
import validateParamIdMiddleware from './../../middleware/validateParamId.middleware'
import validationMiddleware from './../../middleware/validation.middleware';
import { plainToClass } from 'class-transformer';
import { isAllowed } from './../../middleware/acl.middleware';
import HttpException from './../../exceptions/HttpException';

class OrganizationController implements Controller {

  public path = '/organization';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getOrganizations);
    this.router.get(`${this.path}/:id`, validateParamIdMiddleware, this.getOrganization);
    this.router.post(`${this.path}`,
      validationMiddleware(CreateOrganizationDto),
      isAllowed('organization', null, 'create'),
      this.createOrganization);
    this.router.put(`${this.path}/:id`,
      validateParamIdMiddleware,
      validationMiddleware(CreateOrganizationDto, true),
      isAllowed(Organization, null, 'update'),
      this.updateOrganization);
    this.router.delete(`${this.path}/:id`,
      validateParamIdMiddleware,
      isAllowed(Organization, null, 'delete'),
        this.deleteOrganization);
  }

  /**
   *
   * List organizations
   *
   **/
  private async getOrganizations(_req: express.Request, res:express.Response, next: express.NextFunction) {

    let organizations: Organization[];

    try {
      organizations = await getRepository(Organization).find();
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(organizations);
  }

  /**
   *
   * Show a organization
   *
   **/
  private async getOrganization(req: express.Request, res:express.Response, next: express.NextFunction) {

    let organization: Organization;

    // Find the organization
    try {
      organization = await getRepository(Organization).findOne({
        where: { id: req.params.id },
        relations: ['tickets']
      });
    } catch(err) {
      return next(new HttpException(400, err));
    }

    // Return the result
    res.json(organization);
  }

  /**
   *
   * Create a organization
   *
   **/
  private async createOrganization(req: express.Request, res:express.Response, next: express.NextFunction) {

    let organization: Organization;
    // Convert incoming body to new Order (Only exposed values in DTO)
    let organizationData: CreateOrganizationDto = plainToClass(CreateOrganizationDto, req.body, { excludeExtraneousValues: true });
    sanitize(organizationData);

    try {
      organization = getRepository(Organization).create(organizationData);
      organization = await getRepository(Organization).save(organization);
    } catch (err) {
      return next(new HttpException(400, err));
    }

    res.json(organization);
  }

  /**
   *
   * Update a organization
   *
   **/
  private async updateOrganization(req: express.Request, res:express.Response, next: express.NextFunction) {

    let result: Organization;

    // fetch organization and update
    try {
      let organization = await getRepository(Organization).findOne(req.params.id)
      if (!organization) { return res.status(403).json('Organization not found'); }
      getRepository(Organization).merge(organization, req.body);
      sanitize(organization);
      result = await getRepository(Organization).save(organization);
    } catch (err) {
      return next(new HttpException(400, err));
    }

    // Send the result back
    res.json(result);
  }

  /**
   *
   * Delete a organization
   *
   **/
  private async deleteOrganization(req: express.Request, res:express.Response, next: express.NextFunction) {

    const id = parseInt(req.params.id);

    // Fetch the item and delete
    try {
      const toRemove = await getRepository(Organization).findOne(id);
      await getRepository(Organization).remove(toRemove);
    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(true);
  }
}

export default OrganizationController;
