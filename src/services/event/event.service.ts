import { getRepository } from 'typeorm';
import Event from '../../models/event/event.entity';
import Order from '../../models/order/order.entity';
import Ticket from '../../models/ticket/ticket.entity';
// import User from './../user/user.entity';

class IAttendee {
  nickname: String
  picture: String
  type: String
  order_date: Date
}

/**
 *
 * Get user's ticket status for a specific event
 *
 **/
export async function hasOrder(eventId: number, sub: string): Promise<Order> {

  const order = await getRepository(Order).createQueryBuilder('order')
    .leftJoinAndSelect('order.event', 'event')
    .leftJoinAndSelect('order.user', 'user')
    .where('user.auth0 = :sub', { sub: sub })
    .andWhere('event.id = :id', { id: eventId })
    .andWhere('order.status IN (:...statuslist)', { statuslist: ['draft', 'pending', 'completed'] })
    .getOne();

  return order;
}
/**
 *
 * Get attendees for a specific event
 *
 **/
export async function getAttendees(eventId: Number): Promise<IAttendee[]> {
  console.log('[ event.service ] - getAttendees() ');

  let attendees: IAttendee[] = [];
  let event: Event;
  try {
    event = await getRepository(Event).createQueryBuilder('event')
    .leftJoinAndSelect('event.tickets', 'ticket')
    .leftJoinAndSelect('ticket.orders', 'order')
    .leftJoinAndSelect('order.user', 'user')
    .where('event.id = :id', { id: eventId })
    .andWhere('order.status = :status', { status: 'completed' })
    .getOne();
  } catch(err) {
    console.warn(err);
  }

  console.log('[ event.service ] - getAttendees() Restructure orders and attendees');
  if (event && event.tickets) {
    event.tickets.forEach((ticket: Ticket) => {
      ticket.orders.forEach((order: Order) => {
        const attendee: IAttendee = {
          nickname: order.user.nickname,
          picture: order.user.picture,
          type: order.user.account_plan,
          order_date: order.order_date
        }
        attendees.push(attendee)
      });
    });
  }

  console.log('[ event.service ] - getAttendees() return result');
  return attendees;
}
