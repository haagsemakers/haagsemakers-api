import { getRepository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import Activity from '../../models/activity/activity.entity';
import CreateActivityDto from '../../models/activity/activity.dto';
import logger from '../../utils/logger';

/**
 *
 * Save a new payment in the database
 *
 **/
const createActivity = async (data: CreateActivityDto) => {
  logger.info(`[ activity.service ] - Starting to create a new activity item`);

  let activity: Activity;
  try {
    let activityDto: CreateActivityDto = plainToClass(CreateActivityDto, data);
    activity = getRepository(Activity).create(activityDto);
    activity = await getRepository(Activity).save(activity);
  } catch (err) {
    console.warn(err);
    Promise.reject(err);
  }
  console.log(activity)
  return activity;
}

export {
  createActivity
}
