import * as express from 'express';
import User from '../models/user/user.entity';
import HttpError from '../exceptions/HttpException';
import { fetchUser } from '../services//user/user.service';

export function isAllowed(type: any, resource: any, permission: any) {
  return async (req: express.Request, _res: express.Response, next: express.NextFunction) => {

    console.log(`Check is current user with ${req.user.sub} can ${permission} for ${type} with id ${resource}`);
    let user: User;

    // get user
    try {
      user = await fetchUser(req.user.sub);
      if (!user) { return next(new HttpError(400, 'user not found')) }
    } catch (err) {
      return next(new HttpError(400, err));
    }

    // check is user is admin
    if (user.admin) { return next(); }

    // fetch resource
    switch (type) {
      case 'event':
      case 'order':
      case 'ticket':
      default: return next(new HttpError(400, 'User not allowed'));
    }

    // validate resource permission for this user

    next(new HttpError(400, 'User not allowed'));
  }
}
