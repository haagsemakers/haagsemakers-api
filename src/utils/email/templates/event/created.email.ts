
class iEmailData {
  to: string
  event: {
    slug: string
  }
}

export default (data: iEmailData) => {

    const to = data.to;
    const subject = 'New event created on Haagse Makers';
    const html = `<p>Congratulations, you created a new event. <a href="http://haagsemakers.nl/${data.event.slug}">Go check it out!</a></p>`
    const plain = 'event created';

    return { to, subject, html, plain }
}
