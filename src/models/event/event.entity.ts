import {
  AfterLoad,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsEnum,
  IsInt,
  IsPositive,
  IsString,
  IsUrl,
  Length } from 'class-validator';
import {
  Escape,
  Trim } from "class-sanitizer";
import Ticket from '../../models/ticket/ticket.entity';
import Order from './../order/order.entity';
import Organization from './../organization/organization.entity';
import EventEditors from  './event_editors.entity';
import Venue from '../venue/venue.entity';

export enum EventStatus {
  DRAFT = 'draft',
  LIVE = 'live',
  STARTED = 'started',
  ENDED = 'ended',
  COMPLETED = 'completed',
  CANCELED = 'canceled'
}

@Entity()
class Event {

  @PrimaryGeneratedColumn()
  readonly id: number;

  // Event name.
  @Column({
    comment: 'The public name of the event'
  })
  @IsString() @Trim() @Escape() @Length(5,150)
  public name: string;

  @Column({
    comment: 'The public summary of the event (optional)',
    nullable: true
  })
  @IsString() @Trim() @Escape() @Length(20,250)
  public summary: string;

  @Column({
    comment: 'Event description, can be lengthy and have significant formatting.',
    nullable: true,
    type: 'text'
  })
  @IsString() @Trim() @Escape() @Length(20,500)
  public description: string;


  @Column({
    comment: 'url of the events Listing page on haagsemakers.nl',
    nullable: true })
  @IsString() @IsUrl()
  public url: string;

  // Event start date and time.
  @Column({
    comment: 'The startdate and time of the event',
    type: 'timestamptz'
  })
  public start: Date

  @Column({
    comment: 'The enddate and time of the event. ',
    type: 'timestamptz'
  })
  public end: Date

  @Column({
    comment: 'The date the event was published (status changed to live)',
    nullable: true,
    type: 'timestamptz'
  })
  public published: Date

  // Event status. Can be draft, live, started, ended, completed and canceled.
  @Column({
    type: 'enum',
    enum: EventStatus,
    default: EventStatus.DRAFT
  })
  @IsEnum(EventStatus)
  public status: string

  @Column({ nullable: true })
  @IsString()
  @IsUrl()
  public logo: string

  @Column({ nullable: true })
  @IsString()
  public slug: string

  @Column({ default: 1 })
  @IsInt() @IsPositive()
  public max_tickets_per_user: number

  @Column({ default: 100 })
  @IsInt() @IsPositive()
  public capacity: number

  @Column({ default: 0, nullable: true })
  @IsInt() @IsPositive()
  public sold: number

  protected tickets_available: number

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
    createdAt: Date
  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  public updatedAt: Date

  @ManyToOne(() => Venue, venue => venue.events)
  @JoinColumn({ name: 'venue_id' })
  public venue: Venue;

  // An event can have multiple tickets
  @OneToMany(() => Ticket, ticket => ticket.event)
  @JoinColumn({ name: 'ticket_id' })
  public tickets: Ticket[]

  // An event can have multiple orders
  @OneToMany(() => Order, order => order.event)
  @JoinColumn({ name: 'order_id' })
    orders: Order[]

  // An event can belong to multiple organizations
  @ManyToMany(() => Organization, organization => organization.events)
  @JoinColumn({ name: 'organization_id' })
    organizers: Organization[]

  // An event can have multiple editors with a role
  @OneToMany(() => EventEditors, (eventEditors) => eventEditors.user)
  @JoinColumn({ name: 'editor_id' })
    editors!: EventEditors[];

  @AfterLoad()
  getTicketsAvailable() {
    this.tickets_available = this.capacity - this.sold
  }
}

export default Event;
