var ManagementClient = require('auth0').ManagementClient;
import { config } from '../../config';
import logger from '../../utils/logger';

// Initiate auth0 management client
let auth0 = new ManagementClient({
  domain: config.auth0.domain,
  clientId: config.auth0.clientId,
  clientSecret: config.auth0.clientSecret
});

interface ISocial {
  url?: string
  title?: string
}
// User interface
interface IAuth0User {
  user_id?: string
  email?: string
  nickname?: string
  picture?: string
  created_at?: Date
  app_metadata?: {
    slug?: string
    account_plan?: string
    account_plan_end?: Date
    admin?: boolean
  }
  user_metadata?: {
    firstname?: string
    lastname?: string
    bio?: string
    visibility?: string
    socials?: ISocial[ ]
  }
}

//
interface IGetUsersParams {
  per_page?: number
  page?: number
  include_totals?: boolean
  q?: object
}

/**
 *
 * Fetch a user from auth0
 *
 **/
const getAuth0User = async (user_id: string): Promise<any> => {
  return await auth0.getUser({ id: user_id });
}

/**
 *
 * Fetch all users from auth0
 *
 **/
const getAuth0Users = async (q: IGetUsersParams = {}): Promise<any> => {
  logger.debug('[ auth0.service ] - Get users from auth0', q);

  let hasMore = true;
  let result = {
    start: 0,
    limit: 0,
    length: 0,
    total: 0
  }
  let params = {
    per_page: 50,
    page: 0,
    include_totals: true,
    search_engine: 'v3'
  }
  let users: IAuth0User[] = [];

  // Fetch all results while there are still more result
  while (hasMore) {

    const queryResult = await auth0.getUsers(params);

    result.start = queryResult.start;
    result.limit = queryResult.limit;
    result.length = queryResult.length;
    result.total = queryResult.total;

    params.page++;

    users = users.concat(queryResult.users);

    if ( result.length < result.limit) {
      hasMore = false;
    }
  }
  return users;
}

/**
 *
 * Update nickname at auth0;
 *
 **/
const updateAuth0User = async (user_id: string, data: IAuth0User): Promise<any> => {
  const params = { id: user_id }
  let result:any;

  try {
    result = await auth0.updateUser(params, data);
    // if (data.nickname || data.picture) {
    //   const auth0Result = await updateAuth0User(user_id, { nickname: data.nickname, picture: data.picture });
    //   console.log('authResult',auth0Result);
    // } else {
    //   console.log('core fields not changed')
    // }
    // if (Object.keys(data.user_metadata).length>0) {
    //   const result = await auth0.updateAppMetadata(params, data.user_metadata);
    //   console.log(result);
    // }
    // if (Object.keys(data.app_metadata).length>0) {
    //   const result = await auth0.updateUserMetadata(params, data.app_metadata);
    //   console.log(result);
    // }
  } catch(err) {
    console.log(err);
  }
  console.log('updateAuth0User done');

  return result;
}

/**
 *
 * Check if nickname is available at auth0.
 *
 **/
const nicknameIsAvailable = async (nickname: string): Promise<boolean> => {
  let users:any;
  let result: boolean = false;
  try {
    users = await auth0.getUsers({nickname: nickname });
    console.log(users);
    if (!users) { result = true; }
  } catch (err) {
    console.log(err);
  }
  return result;
}

/**
 *
 * Cast any object into an auth0 user.
 *
 **/
const castToAuth0 = (incoming: any): IAuth0User => {

  let auth0User: IAuth0User = { app_metadata: {}, user_metadata: {} };

  // Add fields if present
  if (incoming.nickname) { auth0User.nickname = incoming.nickname; }
  if (incoming.picture) { auth0User.picture = incoming.picture; }
  if (incoming.app_metadata) {
    if (incoming.app_metadata.slug) { auth0User.app_metadata.slug = incoming.app_metadata.slug; }
  }
  if (incoming.user_metadata) {
    if (incoming.user_metadata.firstname)   { auth0User.user_metadata.firstname  = incoming.user_metadata.firstname;  }
    if (incoming.user_metadata.lastname)    { auth0User.user_metadata.lastname   = incoming.user_metadata.lastname;   }
    if (incoming.user_metadata.bio)         { auth0User.user_metadata.bio        = incoming.user_metadata.bio;        }
    if (incoming.user_metadata.visibility)  { auth0User.user_metadata.visibility = incoming.user_metadata.visibility; }
  }

  return auth0User;
}

/**
 *
 * Check if user has a certain role.
 *
 **/
const hasRole = async (sub: string, role: string): Promise<boolean> => {
  const profile = await auth0.getUser({id: sub});
  logger.info({ message: 'User received', profile: profile });
  if (!profile) { return false; }

  const roles = (profile.app_metadata || {}).roles || [];

  return (roles.indexOf(role) !== -1);
}

// Exports
export {
  IAuth0User,
  ISocial,
  getAuth0User,
  getAuth0Users,
  updateAuth0User,
  castToAuth0,
  nicknameIsAvailable,
  hasRole
}
