import * as express from 'express';
import { getRepository } from 'typeorm';
import { sanitize } from 'class-sanitizer';
import Controller from './../../interfaces/controller.interface';
import validationMiddleware from './../../middleware/validation.middleware';
import validateParamIdMiddleware from './../../middleware/validateParamId.middleware';
import CreateTicketDto from '../../models/ticket/ticket.dto';
import Ticket from '../../models/ticket/ticket.entity';
import Event from '../../models/event/event.entity';
import logger from './../../utils/logger';
import HttpException from './../../exceptions/HttpException';
import { plainToClass } from 'class-transformer';
import { isAllowed } from './../../middleware/acl.middleware';

class TicketController implements Controller {

  public path = '/ticket';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, this.getTickets);
    this.router.get(`${this.path}/:id`, this.getTicket);

    this.router.post(this.path, validationMiddleware(CreateTicketDto), isAllowed(Ticket, null, 'create'), this.createTicket);
    this.router.put(`${this.path}/:id`, validateParamIdMiddleware, validationMiddleware(CreateTicketDto, true), isAllowed(Ticket, null, 'update'), this.updateTicket);
    this.router.delete(`${this.path}/:id`, validateParamIdMiddleware, isAllowed(Ticket, null, 'delete'), this.deleteTicket);
  }

  /**
   *
   * List tickets
   *
   **/
  private async getTickets(_req: express.Request, res:express.Response) {

    let tickets: Ticket[];

    try {
      tickets = await getRepository(Ticket).find({
        relations: [ 'event' ]
      });
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(tickets);
  }

  /**
   *
   * Show a ticket
   *
   **/
  private async getTicket(req: express.Request, res:express.Response) {

    let ticket: Ticket;

    try {
      ticket = await getRepository(Ticket).findOne(req.params.id, {
        relations: [ 'event' ]
      });
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.json(ticket);
  }

  /**
   *
   * Create a ticket (connected to an event)
   *
   **/
  private async createTicket(req: express.Request, res:express.Response, next: express.NextFunction) {

    let event: Event;
    let ticket: Ticket;

    // Convert incoming body to new Order (Only exposed values in DTO)
    let ticketData: CreateTicketDto = plainToClass(CreateTicketDto, req.body, { excludeExtraneousValues: true });
    sanitize(ticketData);

    try {
      // Make sure the event exists
      event = await getRepository(Event).findOne(req.body.event);
      if (!event) {
        return next(new HttpException(404, `Event (id: ${req.body.event}) not found`));
      }

      // Create the new ticket instance
      ticket = getRepository(Ticket).create(ticketData);
      // Save the ticket in the database
      await getRepository(Ticket).save(ticket);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    // Send back the result
    res.send(ticket);
  }

  /**
   *
   * Update a ticket
   *
   **/
  private async updateTicket(req: express.Request, res:express.Response) {

    let result: Ticket;

    try {
      let ticket = await getRepository(Ticket).findOne(req.params.id)
      if (!ticket) { return res.status(403).json('Ticket not found'); }
      getRepository(Ticket).merge(ticket, req.body);
      sanitize(ticket);
      result = await getRepository(Ticket).save(ticket);
    } catch (err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    res.send(result);
  }

  /**
   *
   * Delete a ticket
   *
   **/
  private async deleteTicket(req: express.Request, res:express.Response) {

    console.log('Delete a ticket', req.params.id);
    const id = parseInt(req.params.id);

    try {
      const toDelete = await getRepository(Ticket).findOne(id);
      await getRepository(Ticket).remove(toDelete);
    } catch(err) {
      logger.warn(err);
      return res.status(500).json(err);
    }

    return res.json(true);
  }
}

export default TicketController;
