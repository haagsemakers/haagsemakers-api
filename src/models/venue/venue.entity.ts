/*
address	address	Venue address.
id	string	Venue ID.
age_restriction	string	Age restriction of the Venue.
capacity	number	Maximum number of tickets that can be sold for the Venue.
name	string	Venue name.
latitude	string	Latitude coordinates of the Venue address.
longitude	string	Longitude coordinates of the Venue address.
*/

import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsString,
  IsUrl,
  Length,
  MaxLength } from 'class-validator';
import {
  Escape,
  Trim } from "class-sanitizer";
import Event from './../event/event.entity';

@Entity()
class Venue {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  @IsString()
  @Trim()
  @Escape()
  @Length(5,250)
  public name: string;

  @Column({ nullable: true })
  @IsString()
  @Trim()
  @Escape()
  @MaxLength(250)
  public address: string;

  // (Optional) Description can be lengthy and have significant formatting.
  @Column({ nullable: true, type: 'text' })
  @IsString()
  @Trim()
  @Escape()
  @MaxLength(500)
  public description: string;

  @Column({ nullable: true })
  public url: string;

  // link to logo url
  @Column({ nullable: true })
  @IsString()
  @IsUrl()
  public logo: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  public createdAt: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  public updatedAt: Date

  @OneToMany(() => Event, event => event.venue)
  public events: Event[];
}

export default Venue;
