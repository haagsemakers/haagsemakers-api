import * as express from 'express';
import Controller from './../../interfaces/controller.interface';

class HomeController implements Controller {

  public path = '/';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.getHome);
  }

  private async getHome(_req: express.Request, res:express.Response, _next: express.NextFunction) {

    let p = require('./../../../package.json');
    
    res.json({
      name: p.name,
      version: p.version,
      description: p.description,
      repository: p.repository
    });
  }
}

export default HomeController;
