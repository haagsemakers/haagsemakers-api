import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';
import {
  Length,
  Max,
  Min
} from 'class-validator';
import User from '../user/user.entity';
import Order from '../order/order.entity';

@Entity()
class Payment {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: true
  })
  @Length(3, 150)
  description: string;

  @Column({ nullable: false })
  @Min(1)
  @Max(10000)
  cost: number

  @Column({ nullable: false })
  @Min(1)
  @Max(10000)
  fee: number

  get amount(): number { return (this.cost + this.fee ); }

  @Column({ default: 'draft' })
  status: string

  @Column({ nullable: true })
  mollie_id: string

  @Column({ nullable: true, type: 'timestamp with time zone'})
  mollie_expires: Date

  @Column({
    nullable: true,
    type: 'text' })
  mollie_details: string

  @CreateDateColumn({ name: 'created_at', type: 'timestamp with time zone' })
  createdAt: Date

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp with time zone' })
  updatedAt: Date

  // A user can have multiple payments
  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  // A payment belongs to 1 order
  @ManyToOne(() => Order, order => order.payments)
  @JoinColumn({ name: 'order_id' })
  order: Order;
}

export default Payment;
