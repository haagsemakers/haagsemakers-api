import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm';

export enum ActivityAction {
  CREATE = 'create',
  RETRIEVE = 'retrieve',
  UPDATE = 'update',
  DELETE = 'delete'
}

@Entity()
class Activity {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column({
    type: 'enum',
    enum: ActivityAction
  })
  action: string

  @Column()
  description: string;

  @Column({ type: 'jsonb', nullable: true })
  data: object;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date


}

export default Activity;
