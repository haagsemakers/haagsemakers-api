
class iEmailData {
  to: {
    email: string
    name: string
  }
  event: {
    name: string
    slug: string
  }
  ticket: {
    id: number
  }
}


export default (data: iEmailData) => {

  const to = data.to.email;
  const subject = `[Haagse Makers] Bevestiging annuleren van je reservering : ${data.event.name}`;
  const html = `
    <h1>Annulering reservering</1>
    <h2>${data.event.name}</h2>
    <p>Dit is een bevestiging van het annuleren van je bestelling van de bijeenkomst: ${data.event.name} (Ticket: ${data.ticket.id})</p>
    <br>
    <p><a href="http://haagsemakers.nl/${data.event.slug}">Ga naar de event pagina</a></p>
    <br>
    <p>Veel plezier, Haagse Makers</p>`;

  const plain = 'Annulering reservering';

  return { to, subject, html, plain }
}
