import {
  IsDate,
  IsInt,
  IsPositive,
  IsString,
  Length,
  MaxLength,
  IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { Expose } from 'class-transformer';
import Event from './../event/event.entity';
import Order from './../order/order.entity';
import TicketRoleRequired from './ticket.entity'; 

class CreateTicketDto {

  @Expose()
  @IsString()
  @Length(3, 150)
  public name: string;

  @Expose()
  @IsOptional()
  @MaxLength(150)
  public description: string;

  @Expose()
  @IsOptional()
  @IsInt()
  @IsPositive()
  @Type(() => Number)
  public cost: number

  @Expose()
  @IsOptional()
  @IsInt()
  @IsPositive()
  @Type(() => Number)
  public fee: number

  @IsOptional()
  // @IsEnum(EventStatus) // IsEnum checking for strings seems to be not working at this point.
  @Type(()=> TicketRoleRequired)
  public role_required: string

  // @Expose()
  // @IsInt()
  // @IsPositive()
  // @IsOptional()
  // @Type(() => Number)
  // public minimum_quantity: number;
  //
  // @Expose()
  // @IsInt()
  // @IsPositive()
  // @IsOptional()
  // @Type(() => Number)
  // public maximum_quantity: number;

  @Expose()
  @IsOptional()
  @IsInt()
  @IsPositive()
  @Type(()=> Number)
  public capacity: number;

  @Expose()
  @IsDate()
  @IsOptional()
  @Type(()=> Date)
  public sales_start: Date

  @IsDate()
  @IsOptional()
  @Type(()=> Date)
  public sales_end: Date

  @Expose()
  @IsInt()
  @IsPositive()
  @Type(() => Number)
  public event: Event

  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
  public orders: Order[];
}

export default CreateTicketDto;
