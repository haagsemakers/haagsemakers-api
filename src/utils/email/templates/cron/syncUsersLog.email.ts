
class iEmailData {
  to: string
  subject?: string
  content?: {
    total: number
    syncedUsers: [{
      nickname: string
      user_id: number
    }]
  }
}

export default (data: iEmailData) => {
  const to = data.to;
  const subject = 'User sync report for haagsemakers.nl API';

  let html = `<p>This is the report for the user sync from auth0 to the Haagse Makers Backend</p>`;
      html += `<p>Number of users fetched from auth0: ${data.content.total}</p>`;
      html += `<p>Number of users synchronized from auth0: ${data.content.syncedUsers.length}</p>`;
      if (data.content.syncedUsers.length >0) {
        html += '<ul>';
        data.content.syncedUsers.map((user:any) => {
          html += `<li>${user.nickname} (${user.user_id})</li>`;
        })
        html += '</ul>';
      }
  const plain = 'sync report';

  return { to, subject, html, plain };
}
