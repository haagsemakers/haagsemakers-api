/*
id	string	Organization ID.
name	string	Organization Name.
image_id	string	(Optional) ID of the image for an Organization.
vertical	string	Type of business vertical within which this Organization operates. Currently, the only values are default and music. If not specified, the value is default.
*/

import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import {
  IsString,
  IsUrl,
  Length,
  MaxLength } from 'class-validator';
import {
  Escape,
  Trim } from "class-sanitizer";
import Event from './../event/event.entity';

@Entity()
class Organization {

  @PrimaryGeneratedColumn()
  readonly id: number;

  // Event name.
  @Column()
  @IsString() @Length(5,250)
  @Trim() @Escape()
  public name: string;

  // (Optional) Short summary describing the event and its purpose.
  @Column({ nullable: true })
  @IsString() @MaxLength(250)
  @Trim() @Escape()
  public summary: string;

  // (Optional) Description can be lengthy and have significant formatting.
  @Column({ nullable: true, type: 'text' })
  @IsString() @MaxLength(500)
  @Trim() @Escape()
  public description: string;

  // URL of the Event's Listing page on haagsemakers.nl
  @Column({ nullable: true })
  @IsString() @IsUrl()
  public url: string;

  // link to logo url
  @Column({ nullable: true })
  @IsString()
  public logo: string;

  @Column({ nullable: true })
  @IsString() @MaxLength(250)
  @Trim() @Escape()
  public location: string

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
    createdAt: Date
  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  public updatedAt: Date

  // An organization can have one or more events, an event can belong to more organizations (ManyToMany)
  @ManyToMany(() => Event, event => event.organizers)
  @JoinTable({
    name: 'event_organizers',
    joinColumn: {
      name: 'organization_id',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'event_id',
      referencedColumnName: 'id'
    }
  })
  public events: Event[];

}

export default Organization;
