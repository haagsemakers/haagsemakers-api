import { Type } from 'class-transformer';
import {
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
  MaxLength } from 'class-validator';
import { Expose } from 'class-transformer';
import Event from './../event/event.entity';

class CreateOrganizationDto {

  @Expose()
  @IsString()
  @MaxLength(150)
    name: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(250)
    summary: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(500)
    description: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(250)
    location: string;

  @Expose()
  @IsOptional()
  @IsString()
  @IsUrl()
    url: string;

  @Expose()
  @IsOptional()
  @IsString()
  @IsUrl()
    logo: string

  @Expose()
  @IsOptional()
  @IsString()
    organizer: string

  @Expose()
  @IsInt({ each: true })
  @IsPositive({ each: true })
  @Type(() => Number)
    events: Event[];
}

export default CreateOrganizationDto;
