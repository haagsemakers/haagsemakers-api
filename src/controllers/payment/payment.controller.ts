import { Request, Response, NextFunction, Router } from 'express';
import { getRepository } from 'typeorm';
import Controller from '../../interfaces/controller.interface';
import Payment from '../../models/payment/payment.entity';
import User from '../../models/user/user.entity';
import Order from '../../models/order/order.entity';
import logger from './../../utils/logger';
import { getMolliePayment } from '../../services/mollie';
import { createPayment } from '../../services/payment/payment.service';
import HttpException from '../../exceptions/HttpException';

class PaymentController implements Controller {
  public path = '/payment';
  public router = Router();
  private paymentRepository = getRepository(Payment);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    // this.router.get(this.path, this.getPayments);
    this.router.get(`${this.path}/:id`, this.getStatus);
    this.router.post(`${this.path}/mollie-hook`, this.mollieHook);
    this.router.post(this.path, this.createPayment);
  }

  //
  // private getPayments = async (request: express.Request, response: express.Response, _next: express.NextFunction) => {
  //
  //   logger.log('info', 'payment controller index ', request.user);
  //   const result = await this.paymentRepository.find({
  //     where: { account_id: request.user.sub },
  //     order: { id: 'DESC' }
  //   });
  //   response.json(result);
  //
  //
  // }

  /**
   *
   * Get Payment status, Update in db if needed and return result
   *
   **/
  private getStatus = async (req: Request, res: Response, next: NextFunction) => {

    logger.debug(`[ payment.controller getStatus ] Find payment ${req.params.id} for user ${req.user.sub}. `);
    let result;

    try {
      // Fetch current user
      const user = await getRepository(User).findOne({
        where: {
          auth0: req.user.sub
        }
      })
      if (!user) { return next(new HttpException(404, 'User not found'))}

      // Fetch payment
      const payment = await this.paymentRepository.findOne({
        where: {
          id: req.params.id,
          user_id: user.id
        }
      });
      if (!payment) { return next(new HttpException(404, 'Payment not found'))}
      logger.debug(`[ payment.controller getStatus ] Payment ${payment.id} found`);

      // check status at mollie
      logger.debug(`[ payment.controller getStatus ] Fetching mollie status for payment ${payment.id} with mollie_id: ${payment.mollie_id}`);
      let mollieResult = await getMolliePayment(payment.mollie_id);
      if (!mollieResult) { return next(new HttpException(404, 'Mollie item not found'))}

      // Update status if needed
      if (mollieResult.status !== payment.status) {
        logger.debug(`[ payment.controller getStatus ] Status changed at Mollie from ${payment.status} to ${mollieResult.status}. Starting database update`);
        payment.status = mollieResult.status;
        payment.mollie_details = JSON.stringify(mollieResult);
        await getRepository(Payment).save(payment);
      }

      result = mollieResult;
      const paymentUrl: string = mollieResult.getPaymentUrl();
      logger.debug(`[ payment.controller ] Fetched paymentUrl if exists: ${ paymentUrl }`);
      result.paymentUrl = paymentUrl;

    } catch(err) {
      return next(new HttpException(400, err));
    }

    res.json(result);
  }

  //
  private mollieHook = async (req: Request, res: Response, next: NextFunction) => {
    console.log('Mollie Hook received', req.body);
    if (!req.body.id) { return res.status(400).json('No id provided')}

    const mollie_id = req.body.id;

    // Fetch the payment from mollie with the provided mollie_id
    try {
      const molliePayment = await getMolliePayment(mollie_id);
      logger.debug(`[ payment.controller ] - getMolliePayment result: ${molliePayment.id}`);

      const payment = await this.paymentRepository.findOne({ where: { mollie_id: mollie_id } });
      if (!payment) { return next(new HttpException(403, `Payment with mollie_id ${mollie_id} not found`)); }
      logger.debug(`[ payment.controller ] - local payment id: ${ payment.id }`);
      logger.debug(`[ payment.controller - mollieHook ] Updating status from ${ payment.status } to ${ molliePayment.status }`);

      // Update the payment status in the db
      payment.status = molliePayment.status;
      payment.mollie_details = JSON.stringify(molliePayment);
      getRepository(Payment).save(payment);

      logger.debug(`[ payment.controller - mollieHook ] Payment ${payment.id} updated. `);
    } catch(err) {
      return next(new HttpException(400, err));
    }

    return res.json(true);
  }

  /**
   *
   * Create a new payment for an existing order, if needed
   *
   **/
  private createPayment = async (req: Request, res: Response, next: NextFunction) => {
    // Required: order_id
    if (!req.body.order_id) { return next(new HttpException(400, 'order_id field is required')); }

    // Fetch order
    const user: User = await getRepository(User).findOne({ where: { sub: req.user.sub }});
    if (!user) { return next(new HttpException(404, 'User not found')) }
    const order: Order = await getRepository(Order).findOne({
      where: {
        user_id: user.id,
        id: req.body.order_id
      },
      relations: [ 'ticket', 'payments' ]
    });
    if (!order) { return next(new HttpException(404, 'order not found')); }

    // Validate if there is no active payment
    console.log(order.payments);
    let a: Payment[] = order.payments.filter(payment => payment.status === 'open');
    if (a && a.length>0) {
      return next(new HttpException(400, 'There is an open payment for this order. '))
    }

    // Create new payment
    const newPayment = await createPayment({
      cost: order.ticket.cost,
      fee: order.ticket.fee,
      description: order.ticket.name,
      order: order,
      user: user
    });

    console.log(newPayment)
    order.payments.push(newPayment);

    // Return the result
    return res.json(order);
  }
}



export default PaymentController;
