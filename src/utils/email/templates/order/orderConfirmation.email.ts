
class iEmailData {
  to: {
    email: string
    name: string
  }
  event: {
    name: string
    slug: string
  }
  ticket: {
    id: number
  }
}


export default (data: iEmailData) => {

  const to = data.to.email;
  const subject = `[Haagse Makers] Bevestiging reservering: ${data.event.name}`;
  const html = `
    <h1>Bevestiging reservering</1>
    <h2>${data.event.name}</h2>
    <p>Thank you for your order on Haagse Makers. This is a confirmation for the event: ${data.event.name}. Ticket id: ${data.ticket.id}</p>
    <br>
    <p><a href="http://haagsemakers.nl/${data.event.slug}">Ga naar de event pagina</a></p>
    <br>
    <p>Veel plezier, Haagse Makers</p>`;

  const plain = 'event created';

  return { to, subject, html, plain }
}
