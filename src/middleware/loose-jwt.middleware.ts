import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';

// Try to decode an jwt if present but not required
function looselyAuthenticatedMiddleware(req: Request, _res: Response, next: NextFunction) {

  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(' ');
    const token = parts[ 1];
    try {
      const dtoken: any = jwt.decode(token, {complete: true})
      if (dtoken && dtoken.payload) {
        req.user = dtoken.payload
      }
    } catch(err) {
      console.warn(err)
    }
  }
  next();
}

export default looselyAuthenticatedMiddleware;
