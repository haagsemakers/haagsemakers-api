import {
  IsUrl,
  IsOptional,
  MaxLength,
  IsString,
  Length } from 'class-validator';
import { Expose } from 'class-transformer';

class CreateVenueDto {

  @Expose()
  @IsString()
  @Length(5,250)
  public name: string;

  @Expose()
  @IsString()
  @IsOptional()
  @MaxLength(250)
  public address: string;

  @Expose()
  @IsString()
  @IsOptional()
  @MaxLength(500)
  public description: string;

  @Expose()
  @IsOptional()
  @IsString()
  @IsUrl()
  public url: string;

  @Expose()
  @IsOptional()
  @IsString()
  public logo: string;

}

export default CreateVenueDto;
