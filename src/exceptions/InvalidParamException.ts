import HttpException from './../exceptions/HttpException';

class InvalidParamException extends HttpException {
  constructor(id: string) {
    super(403, `Param ${id} is not valid`);
  }
}

export default InvalidParamException;
