import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  ManyToOne,
  PrimaryGeneratedColumn } from 'typeorm';
import {
  IsEnum,
  MaxLength } from 'class-validator';
import {
  Escape,
  Trim } from "class-sanitizer";
import User from './../user/user.entity';
import Ticket from './../ticket/ticket.entity';
import Event from './../event/event.entity';
// import Skill from './user/skill.entity';
import Payment from '../payment/payment.entity';

export enum OrderStatus {
  DRAFT = 'draft',
  PENDING = 'pending',
  EXPIRED = 'expired',
  COMPLETED = 'completed',
  CANCELED = 'canceled'
}

@Entity()
class Order {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column({
    type: 'timestamptz',
    default: () => 'now()'
  })
  order_date: Date

  @Column({
    type: 'timestamptz',
    default: () => 'now()',
    nullable: true
  })
  time_remaining: Date

  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.PENDING
  })
  @IsEnum(OrderStatus)
  status: string

  @Column({ nullable: true })
  @MaxLength(150)
  @Escape()
  @Trim()
  notes: string

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  createdAt: Date

  // An order belongs to one user
  @ManyToOne(() => User, user => user.orders)
  @JoinColumn({ name: 'user_id' })
  user: User

  // An order has a single ticket. (RSVP)
  @ManyToOne(() => Ticket, ticket => ticket.orders)
  @JoinColumn({ name: 'ticket_id' })
  ticket: Ticket

  // An order has a single event. (RSVP)
  @ManyToOne(() => Event, event => event.orders)
  @JoinColumn({ name: 'event_id' })
  event: Event

  // An order can have multiple payments
  @OneToMany(() => Payment, payment => payment.order)
  @JoinColumn({ name: 'payment_id' })
  payments: Payment[]
  
}

export default Order;
