export { default as eventCreated } from './event/created.email';
export { default as orderConfirmation } from './order/orderConfirmation.email';
export { default as orderCancelation } from './order/orderCancelation.email';
export { default as syncUsersLog } from './cron/syncUsersLog.email';
